#!/usr/bin/python
import time
import subprocess

import pytest

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

pattern_to_ignore = ['*.pyc']

class TestHandler(FileSystemEventHandler):
    def on_modified(self, event):
        if '__pycache__' not in event.src_path:
            print(f'event type: {event.event_type}  path : {event.src_path}')
            # subprocess.run(['pytest', '--pdb', '--log-cli-level', 'debug'])
            subprocess.run(['pytest', '--pdb'])


if __name__ == "__main__":
    # subprocess.run(['pytest', '--pdb', '--log-cli-level', 'debug'])
    subprocess.run(['pytest', '--pdb'])
    event_handler = TestHandler()
    observer = Observer()
    observer.schedule(event_handler, path='./hammamvisor/', recursive=True)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()