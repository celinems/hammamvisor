FROM python:3.6

ENV PYTHONUNBUFFERED 1
ENV DEBIAN_FRONTEND=noninteractive

# Install nodejs
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -a && \
  apt-get install -y nodejs

# Enable Yarn
RUN curl -o- -L https://yarnpkg.com/install.sh | bash

RUN apt-get update && \
  apt-get dist-upgrade -y && \
  apt-get install -y libgdal-dev && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists*

# Allows docker to cache installed dependencies between builds
COPY ./requirements.txt requirements.txt
RUN pip install -r requirements.txt

# Adds our application code to the image
COPY . code
WORKDIR code

EXPOSE 8000

# Migrates the database, uploads staticfiles, and runs the production server
CMD ./manage.py migrate && \
    ./manage.py collectstatic --noinput && \
    gunicorn --bind 0.0.0.0:$PORT --access-logfile - hammamvisor.core.wsgi:application
