
# Open a bash console inside the container running Django.
console:
	docker exec -it hammamvisor_web_1 bash

test-console:
	docker exec -it hammamvisor_test_1 bash

# Open an IPDB breakpoint debugger inside web container.
ipdb-web:
	docker-compose down -v
	docker-compose run --service-ports web

ipdb-test:
# 	docker-compose down -v
	docker-compose run --service-ports test

specific-test:
	# Run it like this:  make specific-test TARGET=hammamvisor/scrappers/tests/fetchers/
	docker-compose run --service-ports test pytest --pdb --pyargs $(TARGET)

# Run Celery on debug an local mode.
debug-celery:
	docker-compose run web python -m celery -A background_jobs.run -l info worker -E

clean:
	docker-compose down --rmi all -v
	sudo find . -name '*.pyc' -delete
	sudo find . -name '__pycache__' -delete
	sudo find . -name '*.pid' -delete
	sudo find . -name '*.log' -delete

up:
	sudo find . -name '*.pid' -delete # remove pid files when working on a local environment
	docker-compose up

chown:
	# If files were created within a Docker container,
	# they cannot be updated by a user outside this container.
	# Change ownership so that I can update a file outside a container.
	sudo chown cms $(TARGET)
	sudo chmod -R 777 $(TARGET)

dump-website-data:
	./manage.py dumpdata website > hammamvisor/website/fixtures/baths.json

load-website-data:
	 ./manage.py loaddata hammamvisor/website/fixtures/baths.json

test-coverage:
	docker-compose run test pytest --cov=hammamvisor
	docker-compose run test coverage html
	opera htmlcov/index.html


# Frontend commands
yarn-install:
	docker-compose run web bash -c "/root/.yarn/bin/yarn install"


send-media:
	rsync -av media celinems@$(IP_ADDRESS):hammamvisor/media