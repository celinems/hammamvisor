import itertools
import io
import os
import pytz
import datetime as dt

from django.db import transaction
from django.conf import settings
from django.core.files.base import ContentFile
from django.db.utils import IntegrityError
from PIL import Image

from website.models import Bath, OpeningHour, Address, Picture
from .adapters.google import PlaceSearchAdapter, PlaceDetailsAdapter, PlacePhotosAdapter



########################################################################
############################### Bath ###################################
########################################################################
class BathFetcher:


    @staticmethod
    def _sanitize_provider_name(baths: dict) -> dict:
        sanitized_baths = []
        for key, group in itertools.groupby(baths, key=lambda x: x['provider']):
            key = key.lower()
            provider = Bath.PROVIDERS.get_value(key)
            for bath in list(group):
                bath['provider'] = provider
                sanitized_baths.append(bath)

        return sanitized_baths


    @classmethod
    def _store_reviews(cls, bath: Bath, reviews: list):
        for review in reviews:
            if cls._is_not_a_duplicate(bath=bath, review=review):
                with transaction.atomic():
                    bath.reviews.create(
                        author_name=review['author_name'],
                        author_picture=review['author_picture'],
                        text=review['text'],
                        rating=review['rating'],
                        date=review['date']
                    )

    @classmethod
    def _store_address(cls, bath: Bath, address: Address):
        with transaction.atomic():
            location = Address.easy_point_field(latitude=address['latitude'],
                                                longitude=address['longitude'])
            address = Address.objects.create(address=address['address'],
                                             postal_code=address['postal_code'],
                                             city=address['city'],
                                             location=location)
            address.bath = bath
            address.save()


    @classmethod
    def _store_pictures(cls, bath: Bath, pictures: list):
        for picture in pictures:
            with transaction.atomic():
                bath.pictures.create(
                    width=picture['width'],
                    height=picture['height'],
                    provider_id=picture['provider_id']
                )


    @staticmethod
    def _is_not_a_duplicate(bath: Bath, review: dict) -> bool:
        if isinstance(review['date'], str):
            review['date'] = dt.datetime.strptime(
                review['date'],
                '%Y-%m-%dT%H:%M:%S')

        formated_date = dt.datetime.strftime(review['date'], '%Y-%m-%d')
        count = bath.reviews.filter(date__date=formated_date)\
                            .filter(author_name=review['author_name']).count()
        if count == 0:
            return True


    @classmethod
    def get_baths(cls) -> dict:
        google_adapter = PlaceSearchAdapter()
        google_baths = google_adapter.get_spas_in_ile_de_france()
        baths = cls._sanitize_provider_name(google_baths)
        return baths


    @classmethod
    def get_bath_details(cls, provider_id: str) -> dict:
        try:
            return PlaceDetailsAdapter().get_details(provider_id)
        except Exception as error:
            raise RuntimeError(error)


    @classmethod
    def store_bath_details(cls, provider_id: str, details: dict) -> str:
        bath = Bath.objects.get(provider_id=provider_id)

        # It should break if there is an error.
        with transaction.atomic():
            if details['opening_hours']:
                for i, item in enumerate(details['opening_hours']):
                    bath.opening_hours.create(
                        week_day=i,
                        opens_at=item['open'],
                        closes_at=item['close']
                    )

        if details['reviews']:
            cls._store_reviews(bath=bath, reviews=details['reviews'])

        if details['address']:
            cls._store_address(bath=bath, address=details['address'])

        if details['pictures']:
            cls._store_pictures(bath=bath, pictures=details['pictures'])

        if details['website']:
            bath.website = details['website']
            bath.booking_url = details['website']

        if details['phone']:
            bath.phone = details['phone']

        if details['rating']:
            bath.rating = details['rating']

        if details['total_reviews']:
            bath.total_reviews = details['total_reviews']

        try:
            with transaction.atomic():
                bath.save()
                msg = 'Bath updated successfully. Name: {}'.format(bath.name)
        except IntegrityError as error:
            msg = f'IntegrityError: {error}'

        return msg




########################################################################
############################# Picture ##################################
########################################################################
class PictureFetcher:
    """
    This class converts any picture in a ready-to-store Picture object.
    It also stores Picture objects in the database.
    """

    @classmethod
    def store_multiple_images(cls, *pictures):
        """
        Instantiates an object for each picture provided and store it in the database.

        Input:
            pictures: a list of dictionaries.
            The latter should look like this:
            {'image': bytes_image, 'bath_id': Bath,
                'attr': {'height': int, 'width': int, 'provider_id': str}
            }

        No output
        """
        for picture in pictures:
            obj = cls(
                image=picture['image'],
                bath_id=picture['bath_id'],
                **picture['attr']
            )
            obj.save_image_in_db()


    @classmethod
    def get_and_store_bath_pictures_from_google(cls, *pictures):
        """
        Executes the `get_and_store_a_bath_picture_from_google` method.
        Input: a list of Picture objects existing in the database.
        """

        for picture in pictures:
            cls.get_and_store_a_bath_picture_from_google(picture.id)


    @classmethod
    def get_and_store_a_bath_picture_from_google(cls, picture_id: str):
        """
        Method calling Google to get an image and tying it to its Picture objects.
        Input: a Picture id.
        """

        picture = Picture.objects.get(pk=picture_id)
        image_attr = PlacePhotosAdapter().image_from_photo_reference(
            height=picture.height,
            width=picture.width,
            provider_id=picture.provider_id
        )
        image = image_attr['image']
        image_attr.pop('image')

        fetcher = cls(image=image,
                      bath_id=picture.bath.id,
                      **image_attr
        )
        fetcher.update_picture_in_db(picture_id=picture.id)


    def __init__(self, image: bytes, bath_id: str, **attr):
        """
        Input:
            'image': bytes
            'bath_id': str
            'attr': {'height': int, 'width': int, 'provider_id': str}
        """

        self.bytes_image = image
        self.bath = Bath.objects.get(pk=bath_id)
        self.type = attr['type']
        self.width = attr.get('width', None)
        self.height = attr.get('height', None)
        self.provider_id = attr.get('provider_id', None)


    def _filename(self, filetype: str) -> str:
        total_files = len(os.listdir(self.bath.folder_name))
        filename = "{}.{}".format(
            total_files,
            filetype
        )
        return filename


    def _path(self):
        filename = self._filename(filetype=self.type)
        return os.path.join(self.bath.folder_name, filename)


    def save_image_in_db(self):
        """
        Stores the self.image in our database as a Picture object.
        """

        path = self._path()

        with transaction.atomic():
            picture = Picture.objects.create(
                bath_id=self.bath.id,
                provider_id=self.provider_id,
                width=self.width,
                height=self.height
            )
            picture.image.save(path, ContentFile(self.bytes_image))

        return picture

    def image_size(self) -> dict:
        """
        Return dimensions of the self.bytes_image picture.
        """

        image = Image.open(io.BytesIO(self.bytes_image))
        return {'height': image.height, 'width': image.width}


    def update_picture_in_db(self, picture_id: str):
        """
        Update an existing Picture object in the database to add a file.
        Input: picture id
        """

        path = self._path()
        picture = Picture.objects.get(pk=picture_id)
        image_size = self.image_size()

        with transaction.atomic():
            picture.height, picture.width = image_size['height'], image_size['width']
            picture.image.save(path, ContentFile(self.bytes_image))

        return picture