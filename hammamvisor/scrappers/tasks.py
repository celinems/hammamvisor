from celery import chord, chain, group, Task
from django.db import transaction
from django.db.models import F
from django.db.utils import IntegrityError

from background_jobs.run import app
from scrappers.adapters.google_pricing_calculator import GooglePricingCalculator
from website.models import Bath, Picture
# from .fetchers import BathFetcher, PictureFetcher

from scrappers.fetchers import BathFetcher, PictureFetcher


class CeleryBaseClass(Task):

    def __init__(self):
        self.calculator = GooglePricingCalculator()


@app.task(base=CeleryBaseClass)
def check_google_budget(call_type):

    check_google_budget.calculator.data_usage(call_type)
    return {'SPENT: ': check_google_budget.calculator.SPENT, 'REMAINING: ': check_google_budget.calculator.REMAINING_BUDGET }




####################################################################
############################# Baths ################################
####################################################################
@app.task(base=CeleryBaseClass)
def get_details_of_google_baths():
    query = Bath.objects.filter(created_at__gte=F('updated_at__date'))
    total_baths = query.count()
    allowed_calls = get_details_of_google_baths.calculator.allowed_calls('place_details', total_baths)

    baths = query[:allowed_calls]

    group(chain(get_details_of_a_single_bath_from_google.s(provider_id=bath.provider_id), \
                update_bath.s(provider_id=bath.provider_id)) for bath in baths)()
    return total_baths



@app.task(base=CeleryBaseClass)
def get_details_of_a_single_bath_from_google(provider_id: str):
    bath_dict = BathFetcher.get_bath_details(provider_id)
    return bath_dict


@app.task(base=CeleryBaseClass)
def update_bath(bath_dict, provider_id):
    msg = BathFetcher.store_bath_details(provider_id=provider_id, details=bath_dict)
    return msg


@app.task
def get_google_baths_place_id():
    baths = BathFetcher().get_baths()
    results = len(baths)

    stored_baths = Bath.objects.count()

    if results != stored_baths:
        provider_ids = Bath.objects.values_list('provider_id', flat=True)
        chord(create_bath_id.s(bath) \
              for bath in baths if bath['provider_id'] not in provider_ids)(count_stored_baths.si())


@app.task
def create_bath_id(bath: dict):
    try:
        with transaction.atomic():
            Bath.objects.create(
                provider=bath['provider'],
                provider_id=bath['provider_id'],
                name=bath['name']
            )
            msg = 'Bath created successfully.'
    except IntegrityError as e:
        # Do not store the same bath twice.
        msg = f'IntegrityError: {e}'
    return {'msg': msg}


@app.task
def count_stored_baths():
    total_baths = Bath.objects.count()
    return {'total_baths': total_baths}


####################################################################
########################### Pictures ###############################
####################################################################

@app.task(base=CeleryBaseClass)
def store_bath_pictures_from_google():
    pictures = Picture.objects.filter(image__exact='')
    total_pictures = pictures.count()
    allowed_calls = store_bath_pictures_from_google.calculator.allowed_calls('photos', total_pictures)

    pictures = pictures[:allowed_calls]

    chord(store_a_bath_picture_from_google.s(picture.id) \
          for picture in pictures)(count_stored_pictures.si())


@app.task
def store_a_bath_picture_from_google(picture_id):
    PictureFetcher.get_and_store_a_bath_picture_from_google(picture_id)


@app.task
def count_stored_pictures():
    total_pictures = Picture.objects.count()
    pictures_with_images = Picture.objects.exclude(image__exact='').count()
    ratio = (pictures_with_images / total_pictures) * 100
    return {'pictures_with_images': f'{ratio}%'}
