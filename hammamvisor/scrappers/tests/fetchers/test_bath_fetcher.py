from pytest import fixture, mark

from scrappers.fetchers import BathFetcher
from website.models import Bath
from . import fetcher_vcr


class TestBathFetcher:

    @fixture(autouse=True, scope='function')
    def set_up(self):
        bath_details = {
            'provider': Bath.PROVIDERS.get_value('google'),
            'provider_id': 'ChIJ_wdOofxt5kcR2Exb7Hf6w3w',
            'name': 'Hammam Les Cent Ciels Paris'
        }

        self.bath = Bath(**bath_details)
        self.provider_id = self.bath.provider_id
        with fetcher_vcr.use_cassette('test_get_details_of_a_single_bath.yaml'):
            self.details = BathFetcher().get_bath_details(provider_id=self.provider_id)


    def test_get_baths_returns_a_list(self):
        with fetcher_vcr.use_cassette('get_baths_returns_list.yaml'):
            baths = BathFetcher().get_baths()
        assert isinstance(baths, list)
        assert baths


    def test_sanitize_provider_name(self):
        baths = [
            {'name': 'Les Cent Ciels', 'provider': 'Google'},
            {'name': 'Les Bains Supérieurs', 'provider': 'google'}
        ]
        sanitized = BathFetcher._sanitize_provider_name(baths)
        for item in sanitized:
            assert item['provider'] == Bath.PROVIDERS.get_value('google')
        assert len(sanitized) == len(baths)


    def test_get_details_of_a_single_bath(self):
        assert isinstance(self.details, dict)
        assert self.details['name'] == 'Hammam Les Cent Ciels Paris'
        assert self.details['opening_hours']
        assert self.details['reviews']
        assert self.details['website']
        assert self.details['phone']
        assert self.details['rating']
        assert self.details['total_reviews']


    @mark.django_db(transaction=True)
    def test_store_bath_reviews(self):
        self.bath.save()
        BathFetcher._store_reviews(bath=self.bath, reviews=self.details['reviews'])
        assert self.bath.reviews.count() == 5
        self.bath.delete()


    @mark.django_db(transaction=True)
    def test_store_address(self):
        self.bath.save()
        BathFetcher._store_address(bath=self.bath, address=self.details['address'])
        assert self.bath.address
        self.bath.delete()


    @mark.django_db(transaction=True)
    def test_store_pictures(self):
        self.bath.save()
        BathFetcher._store_pictures(bath=self.bath, pictures=self.details['pictures'])
        assert self.bath.pictures.count() == 10
        self.bath.delete()


    @mark.django_db(transaction=True)
    def test_store_bath_details(self):
        self.bath.save()
        BathFetcher.store_bath_details(provider_id=self.provider_id, details=self.details)

        self.bath.refresh_from_db()

        assert self.bath.name == self.details['name']
        assert self.bath.opening_hours.count() == 7
        assert self.bath.reviews.count() == 5
        assert self.bath.address
        assert self.bath.pictures
        assert self.bath.website
        assert self.bath.phone
        assert self.bath.rating
        assert self.bath.booking_url
        assert self.bath.total_reviews

        self.bath.delete()


    @mark.django_db(transaction=True)
    def test_store_bath_reviews_do_not_store_twice(self):
        self.bath.save()
        BathFetcher._store_reviews(bath=self.bath, reviews=self.details['reviews'])
        BathFetcher._store_reviews(bath=self.bath, reviews=self.details['reviews'])
        assert self.bath.reviews.count() == 5

        self.bath.delete()


    @mark.django_db(transaction=True)
    def test_is_not_a_duplicate(self):
        self.bath.save()
        res = BathFetcher._is_not_a_duplicate(bath=self.bath, review=self.details['reviews'][0])
        assert res

        BathFetcher._store_reviews(bath=self.bath, reviews=self.details['reviews'][:1])

        res = BathFetcher._is_not_a_duplicate(bath=self.bath, review=self.details['reviews'][0])
        assert res is None

        self.bath.delete()