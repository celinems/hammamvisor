import os
import shutil

from pytest import fixture, mark
from django.utils.text import slugify
from django.conf import settings

from scrappers.fetchers import PictureFetcher
from scrappers.adapters.google import PlacePhotosAdapter
from website.models import Picture, Bath
from . import fetcher_vcr


@mark.django_db(transaction=True)
class TestPictureFetcher:

    @fixture(autouse=True, scope='function')
    def set_up(self):
        self.original_picture = {
            'height': 533,
            'provider_id': 'CmRaAAAA6GvgD-7-tRDI9ChL4vMstpqOgaXHYdc9swor-Z1t8TInPsDAIuW2SZmA30rXBlKcP_DLwkcFmXysceafcac6BUN_NZmQf1h_Rq__QHBi8Z49sWnPZIZmR8eKwoOUXm4iEhA7nxmGQPJkYhgRQnDGpx9xGhQeMd_Puzk_Ubb79De9Xg3wlKHWjQ',
            'width': 800
        }

        with fetcher_vcr.use_cassette('test_get_picture.yaml'):
            self.image_attr = PlacePhotosAdapter().image_from_photo_reference(**self.original_picture)
            self.image = self.image_attr['image']
            self.image_attr.pop('image')

        self.bath = Bath.objects.create(provider=Bath.PROVIDERS.get_value('google'), name='Les Cent Ciels')
        self.fetcher = PictureFetcher(
            image=self.image,
            bath_id=self.bath.id,
            **self.image_attr
        )

        yield

        Picture.objects.all().delete()
        shutil.rmtree(self.bath.folder_name)


    def test_fetcher_attributes(self):
        assert self.fetcher.height == self.image_attr['height']
        assert self.fetcher.width == self.image_attr['width']
        assert self.fetcher.type == self.image_attr['type']
        assert isinstance(self.fetcher.bytes_image, bytes)
        assert self.fetcher.provider_id == self.original_picture['provider_id']
        assert self.fetcher.bath.id == self.bath.id


    def test_filename(self):
        filename = self.fetcher._filename(filetype='jpeg')
        assert 'jpeg' in filename


    def test_picture_path(self):
        path = self.fetcher._path()
        total = len(os.listdir(self.bath.folder_name))
        assert path.endswith(f"{str(total)}.{self.image_attr['type']}")
        assert path.startswith(settings.MEDIA_ROOT)


    def test_store_picture_in_db(self):
        filepath = self.fetcher._path()
        picture = self.fetcher.save_image_in_db()
        assert Picture.objects.count() == 1
        assert os.path.exists(filepath)
        assert picture.image


    def test_store_multiple_images(self):
        pictures = [
            {'image': self.image, 'bath_id': self.bath.id, 'attr': self.image_attr}
            for _ in range(3)
        ]
        PictureFetcher.store_multiple_images(*pictures)
        assert Picture.objects.count() == 3


    def test_update_image(self):
        # Picture object created when retrieving bath details from Google.
        # See BathFetcher().store_bath_details()
        picture = Picture.objects.create(
            bath_id=self.bath.id,
            **self.original_picture
        )

        # Then we should retrieve Google's picture.
        with fetcher_vcr.use_cassette('test_get_picture.yaml'):
            image_attr = PlacePhotosAdapter().image_from_photo_reference(**self.original_picture)
            image = image_attr['image']
            image_attr.pop('image')

        # And update the existing picture with this file.
        fetcher = PictureFetcher(
            image=image,
            bath_id=picture.bath.id,
            **image_attr
        )
        filepath = fetcher._path()
        fetcher.update_picture_in_db(picture_id=picture.id)

        picture.refresh_from_db()

        assert Picture.objects.count() == 1
        assert picture.image
        assert os.path.exists(filepath)


    def test_get_and_store_bath_pictures_from_google(self):
        pictures = [
            {'height': 533,
             'width': 800,
             'provider_id': 'CmRaAAAA3_-Tsg3XxsPFNeLh4TIm3uB_5oeJLQB4gS2FI1smACMXZ8wr_MkVPSkWSaNJcaonXsDZWmBiebGgfraAtFI62kByc1CWlxgkcs9lfYZdVj-O5x6TCJ6lQ4XJ9zD4fJlaEhDVSQme1hP6MplByu8uX3SJGhSgqFeO4E69FZRo5VSdk97sa_jh8g'},
            {'height': 3996,
             'width': 2664,
             'provider_id': 'CmRaAAAA_z-hVgrXokaSrPlK6ncmHnOgZeDdkXCRtuZBRUGdbiHGTc7_EOIBL4Dbo6ZPGplL7E8-MF1IVyL2-oG9fHnAcDMQRdEcM7tJZAhkrB0cgc9v5nEA2CL9gLgvpDUgdnWYEhAto3anZn_vdyom4zysc1ttGhSTWYfHwtcdwfQMStYNeLLxpdyc_A'},
            {'height': 2031,
             'width': 1080,
             'provider_id': 'CmRaAAAABWIo4PrZMF8Id8WAGOU8LQG9k5Dfsx6D08ux2_t3syHFEgECji42Et8vxaqoyIjqG63y1ktTBvQc_LKSZ2RTnLA0yiWvWIRJDflnIfv3b88Ho77VR3aci0I757xEYbY2EhDemgHyIwm20FXAVrdecQA0GhTzk7QhqDF0QAxjHqFZE99ycClCog'}
        ]

        for picture in pictures:
            Picture.objects.create(
                bath_id=self.bath.id,
                **picture
            )

        with fetcher_vcr.use_cassette('get_and_store_bath_pictures_from_google.yaml'):
            pictures = Picture.objects.all()
            PictureFetcher.get_and_store_bath_pictures_from_google(*pictures)

        assert Picture.objects.count() == len(pictures)
        assert self.bath.pictures.count() == len(pictures)
        total_pictures = len(os.listdir(self.bath.folder_name))
        assert total_pictures == len(pictures)

        for picture in self.bath.pictures.all():
            assert picture.image
            assert picture.width == picture.image.width
            assert picture.height == picture.image.height
            assert picture.provider_id
