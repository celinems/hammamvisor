import io

import pytest
from PIL import Image

from scrappers.adapters.google import PlaceSearchAdapter, PlaceDetailsAdapter, PlacePhotosAdapter
from . import google_vcr


class TestPlaceSearchAdapter:

    @pytest.fixture(autouse=True, scope="function")
    def set_up(self):
        """
        Set attributes before starting a new test.
        """
        self.adapter = PlaceSearchAdapter()
        yield
        del self.adapter

    @google_vcr.use_cassette('test_get_establishment_by_name.yaml')
    def test_get_establishments_by_name(self):
        results = self.adapter.get_establishments_by_name('Les+Cent+Ciels')
        assert len(results) == 2

    @google_vcr.use_cassette('test_get_spas.yaml')
    def test_get_spas(self):
        results = self.adapter.get_spas_in_ile_de_france()
        assert len(results) > 2

    @google_vcr.use_cassette('test_get_establishment_by_name.yaml', record_mode='new_episodes')
    def test_request_to_dict(self):
        result = self.adapter.get_establishments_by_name('Les+Cent+Ciels+Paris+11e')[0]
        assert result['provider_id'] == 'ChIJ_wdOofxt5kcR2Exb7Hf6w3w'
        assert result['provider'] == 'google'
        assert result['name'] == 'Hammam Les Cent Ciels Paris'

    # test pagination
    # If more than 2 results are expected, it should return 2 results.
    @google_vcr.use_cassette('test_pagination_two_results.yaml')
    def test_pagination_two_results(self):
        self.adapter.params['query'] = 'Les+Cent+Ciels+Paris'
        results = self.adapter._paginated_request()
        assert len(results) == 2

    # If more than 20 results are expected, it should return more than 20 results.
    @google_vcr.use_cassette('test_pagination_more_than_twenty_results.yaml')
    def test_pagination_more_than_twenty_results(self):
        self.adapter.params['query'] = 'Spa+Paris'
        results = self.adapter._paginated_request()
        assert len(results) > 30

    # Each item in the result should be unique.
    @google_vcr.use_cassette('test_pagination_more_than_twenty_results.yaml')
    def test_pagination_each_item_should_be_unique(self):
        self.adapter.params['query'] = 'Spa+Paris'
        results = self.adapter._paginated_request()
        ids = [res['place_id'] for res in results]
        assert len(set(ids)) == len(ids)

    # It should return a list.
    @google_vcr.use_cassette('test_pagination_two_results.yaml')
    def test_pagination_should_return_a_list(self):
        self.adapter.params['query'] = 'Spa+Paris'
        results = self.adapter._paginated_request()
        assert isinstance(results, list)



class TestPlaceDetailsAdapter:

    @pytest.fixture(autouse=True, scope="function")
    def set_up(self):
        """
        Set attributes before starting a new test.
        """
        self.adapter = PlaceDetailsAdapter()
        self.place_id = 'ChIJ_wdOofxt5kcR2Exb7Hf6w3w'


    def test_format_address(self):
        address = [{'long_name': '7', 'short_name': '7', 'types': ['street_number']},
                   {'long_name': 'Rue de Nemours', 'short_name': 'Rue de Nemours',
                    'types': ['route']},
                   {'long_name': 'Paris', 'short_name': 'Paris',
                    'types': ['locality', 'political']},
                   {'long_name': 'Paris', 'short_name': 'Paris',
                    'types': ['administrative_area_level_2', 'political']},
                   {'long_name': 'Île-de-France', 'short_name': 'Île-de-France',
                    'types': ['administrative_area_level_1', 'political']},
                   {'long_name': 'France', 'short_name': 'FR', 'types': ['country', 'political']},
                   {'long_name': '75011', 'short_name': '75011', 'types': ['postal_code']}]
        location = {'lat': 48.8652527, 'lng': 2.3723758}

        result = PlaceDetailsAdapter._format_address(address, location)
        for i, item in enumerate(address):
            if item['types'][0] == 'street_number':
                assert item['short_name'] in result['address']
            elif item['types'][0] == 'route':
                assert item['short_name'] in result['address']
            elif item['types'][0] == 'locality':
                assert item['short_name'] == result['city']
            elif item['types'][0] == 'postal_code':
                assert item['short_name'] == result['postal_code']

        assert location['lat'] == result['latitude']
        assert location['lng'] == result['longitude']


    def test_translate_opening_hours(self):
        opening_hours = [{'close': {'day': 0, 'time': '2200'},
                             'open': {'day': 0, 'time': '1000'}},
                            {'close': {'day': 1, 'time': '2300'},
                             'open': {'day': 1, 'time': '1700'}},
                            {'close': {'day': 2, 'time': '2300'},
                             'open': {'day': 2, 'time': '1000'}},
                            {'close': {'day': 3, 'time': '2300'},
                             'open': {'day': 3, 'time': '1000'}},
                            {'close': {'day': 4, 'time': '2300'},
                             'open': {'day': 4, 'time': '1000'}},
                            {'close': {'day': 5, 'time': '2300'},
                             'open': {'day': 5, 'time': '1000'}},
                            {'close': {'day': 6, 'time': '2100'},
                             'open': {'day': 6, 'time': '1000'}}]

        results = PlaceDetailsAdapter._translate_opening_hours(opening_hours)

        for i, item in enumerate(results):
            for key, details in item.items():

                # Assert days are ordered.
                assert opening_hours[i][key]['day'] == details['day']

                # Assert they share the same time.
                if i == 6:
                    assert opening_hours[0][key]['time'] == details['time']
                else:
                    assert opening_hours[i + 1][key]['time'] == details['time']



    def test_format_opening_hours(self):
        opening_hours = [{'close': {'day': 0, 'time': '2200'},
                             'open': {'day': 0, 'time': '1000'}},
                            {'close': {'day': 1, 'time': '2300'},
                             'open': {'day': 1, 'time': '1700'}},
                            {'close': {'day': 2, 'time': '2300'},
                             'open': {'day': 2, 'time': '1000'}},
                            {'close': {'day': 3, 'time': '2300'},
                             'open': {'day': 3, 'time': '1000'}},
                            {'close': {'day': 4, 'time': '2300'},
                             'open': {'day': 4, 'time': '1000'}},
                            {'close': {'day': 5, 'time': '2300'},
                             'open': {'day': 5, 'time': '1000'}},
                            {'close': {'day': 6, 'time': '2100'},
                             'open': {'day': 6, 'time': '1000'}}]
        results = PlaceDetailsAdapter._format_opening_hours(opening_hours)

        for i, item in enumerate(results):
            # Assert index is day number
            assert opening_hours[i]['close']['day'] == i

            # Assert close time
            assert opening_hours[i]['close']['time'] == item['close'].strftime('%H%M')
            # assert open time
            assert opening_hours[i]['open']['time'] == item['open'].strftime('%H%M')


    def test_format_reviews(self):
        reviews = [
            {'author_name': 'Damien XXX', 'author_url': 'https://www.google.com/maps/contrib/107402359886016465733/reviews', 'language': 'fr', 'profile_photo_url': 'https://lh6.googleusercontent.com/-k4URLPUXW2E/AAAAAAAAAAI/AAAAAAAAAAc/6fDB14ZmkbQ/s128-c0x00000000-cc-rp-mo/photo.jpg', 'rating': 4, 'relative_time_description': 'il y a un mois', 'text': "Nous avons passé un excellent moment avec ma moitié.\nLe lieu est beau, bien pensé. Le personnel est très courtois et le gommage était parfait. Tout était vraiment très bien.\n\nIl manque une étoile pour la taille du sauna qui permet de tenir à 3/4 personnes. Ca n'a pas été gênant lors de notre séjour aussi est-ce peut-être un peu injuste. Et dans le sauna il manque un sablier pour pouvoir suivre le temps (c'est très important pour être toujours entre 5 et 10 minutes), c'est un accessoire qui ne coûte rien à installer et j'ai entendu d'autres personnes en parler. Je recommande son installation comme dans tous les sauna aussi tôt que possible.\nS'il était possible aussi de faire savoir aux gens que ce genre d'établissements appelle au calme, ce serait un grand plus. Partager les lieux avec trois couples qui parlent comme s'ils étaient au bar m'a beaucoup dérangé. Souvent il suffit de glisser un mot lors de la prise des clés pour éviter cette déconvenue.\n\nJe recommande chaudement cet établissement.", 'time': 1550219608},
            {'author_name': 'Paulo XXX', 'author_url': 'https://www.google.com/maps/contrib/100840522654450530878/reviews', 'language': 'fr', 'profile_photo_url': 'https://lh3.googleusercontent.com/-Li4l6k7hWIw/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rcpey2HLQ2VGsWzFeD3dl-cVIOdxA/s128-c0x00000000-cc-rp-mo/photo.jpg', 'rating': 2, 'relative_time_description': 'il y a un mois', 'text': "Le lieu est beau et propre, le personnel accueillant, mais... pour s'y relaxer, je n'y reviendrais malheureusement pas. Les gens y parlent comme si ils étaient dans leur salon, sa parle haut et fort, sa résonne, on se croirait dans un hall de gare... et c'est vraiment dérangeant quand on cherche un peu de quiétude.. \nc'est la première fois que je vois ça dans un Hammam. Nul part il est écrit qu'il faut respecter la tranquillité des clients.. \nIl serait peut être bien que je pense que l'accueil dise au gens au préalable de parler a voix basse.. \nil faut malheureusement faire  l'éducation des gens ...", 'time': 1549832920}]

        formated_reviews = PlaceDetailsAdapter._format_reviews(reviews)

        assert len(formated_reviews) == 2
        for i, review in enumerate(formated_reviews):
            assert reviews[i]['author_name'] == review['author_name']
            assert reviews[i]['profile_photo_url'] == review['author_picture']
            assert reviews[i]['text'] == review['text']
            assert reviews[i]['rating'] == review['rating']
            assert reviews[i]['time'] == int(review['date'].replace(tzinfo=None).timestamp())

    def test_format_pictures(self):
        original_pictures = [
            {'height': 533,
             'photo_reference': 'CmRaAAAA6GvgD-7-tRDI9ChL4vMstpqOgaXHYdc9swor-Z1t8TInPsDAIuW2SZmA30rXBlKcP_DLwkcFmXysceafcac6BUN_NZmQf1h_Rq__QHBi8Z49sWnPZIZmR8eKwoOUXm4iEhA7nxmGQPJkYhgRQnDGpx9xGhQeMd_Puzk_Ubb79De9Xg3wlKHWjQ',
             'width': 800},
            {'height': 533,
             'photo_reference': 'CmRaAAAA3_-Tsg3XxsPFNeLh4TIm3uB_5oeJLQB4gS2FI1smACMXZ8wr_MkVPSkWSaNJcaonXsDZWmBiebGgfraAtFI62kByc1CWlxgkcs9lfYZdVj-O5x6TCJ6lQ4XJ9zD4fJlaEhDVSQme1hP6MplByu8uX3SJGhSgqFeO4E69FZRo5VSdk97sa_jh8g',
             'width': 800}
        ]
        formated_pictures = PlaceDetailsAdapter._format_pictures(original_pictures)
        for i, pict in enumerate(formated_pictures):
            assert pict['height'] == original_pictures[i]['height']
            assert pict['width'] == original_pictures[i]['width']
            assert pict['provider_id'] == original_pictures[i]['photo_reference']

    def test_format_pictures_if_list_empty(self):
        """
        Assert the method returns an empty list if no picture is given.
        """
        formated_pictures = PlaceDetailsAdapter._format_pictures([])
        assert formated_pictures == []


    @google_vcr.use_cassette('test_get_details.yaml')
    def test_get_details(self):
        result = self.adapter.get_details(self.place_id)
        assert 'Les Cent Ciels' in result['name']
        assert result['opening_hours']
        assert result['address']
        assert result['pictures']
        assert result['website']
        assert result['phone']
        assert result['rating']
        assert result['total_reviews']



class TestPlacePhotosAdapter:

    @pytest.fixture(autouse=True, scope="function")
    def set_up(self):
        self.picture = {
            'height': 533,
            'provider_id': 'CmRaAAAA6GvgD-7-tRDI9ChL4vMstpqOgaXHYdc9swor-Z1t8TInPsDAIuW2SZmA30rXBlKcP_DLwkcFmXysceafcac6BUN_NZmQf1h_Rq__QHBi8Z49sWnPZIZmR8eKwoOUXm4iEhA7nxmGQPJkYhgRQnDGpx9xGhQeMd_Puzk_Ubb79De9Xg3wlKHWjQ',
            'width': 800
        }


    def test_strip_image_type(self):
        image = 'image/png'
        assert PlacePhotosAdapter().strip_image_type(image) == 'png'


    @google_vcr.use_cassette('test_place_photo_adapter.yaml')
    def test_url_from_photo_reference(self):
        image = PlacePhotosAdapter().image_from_photo_reference(**self.picture)

        assert isinstance(image['image'], bytes)
        assert image['type'] == 'jpeg'
        assert image['width'] == self.picture['width']
        assert image['height'] == self.picture['height']
        assert image['provider_id'] == self.picture['provider_id']


    @pytest.mark.skip('Impossible to know what Google will send us...')
    @pytest.mark.parametrize('counter,picture', [
        ['1', {'height': 533,
             'width': 800,
             'provider_id': 'CmRaAAAA3_-Tsg3XxsPFNeLh4TIm3uB_5oeJLQB4gS2FI1smACMXZ8wr_MkVPSkWSaNJcaonXsDZWmBiebGgfraAtFI62kByc1CWlxgkcs9lfYZdVj-O5x6TCJ6lQ4XJ9zD4fJlaEhDVSQme1hP6MplByu8uX3SJGhSgqFeO4E69FZRo5VSdk97sa_jh8g'}],
        ['2', {'height': 3996,
             'width': 2664,
             'provider_id': 'CmRaAAAA_z-hVgrXokaSrPlK6ncmHnOgZeDdkXCRtuZBRUGdbiHGTc7_EOIBL4Dbo6ZPGplL7E8-MF1IVyL2-oG9fHnAcDMQRdEcM7tJZAhkrB0cgc9v5nEA2CL9gLgvpDUgdnWYEhAto3anZn_vdyom4zysc1ttGhSTWYfHwtcdwfQMStYNeLLxpdyc_A'}],
        ['3', {'height': 2031,
             'width': 1080,
             'provider_id': 'CmRaAAAABWIo4PrZMF8Id8WAGOU8LQG9k5Dfsx6D08ux2_t3syHFEgECji42Et8vxaqoyIjqG63y1ktTBvQc_LKSZ2RTnLA0yiWvWIRJDflnIfv3b88Ho77VR3aci0I757xEYbY2EhDemgHyIwm20FXAVrdecQA0GhTzk7QhqDF0QAxjHqFZE99ycClCog'}]
    ])
    def test_image_from_photo_reference(self, counter, picture):
        with google_vcr.use_cassette(f'test_place_photo_adapter-{counter}.yaml'):
            details = PlacePhotosAdapter().image_from_photo_reference(**picture)
        image = Image.open(io.BytesIO(details['image']))

        # From Google API documentation:
        # Both the maxheight and maxwidth properties accept an integer between 1 and 1600.

        if picture['width'] < 1600:
            assert image.width == picture['width']
            assert image.height == picture['height']
