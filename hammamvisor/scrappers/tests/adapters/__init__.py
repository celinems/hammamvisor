import logging

from pathlib import Path

import configurations
import vcr
from django.conf import settings


configurations.setup()

# Return the path to Test directory
TESTS_DIR = Path(__file__).absolute().parent.parent

def fixtures_path(filename):
    """
    Return an absolute path to a desired fixture file.
    """
    return str(TESTS_DIR / 'fixtures' / filename)


"""
########## VCR configuration for tests ##########

All methods using VCR will have their HTTP requests mocked into a JSON file
located in the 'fixtures' repository.

To know more, read the documentation: https://vcrpy.readthedocs.io/en/latest/usage.html
"""
google_vcr = vcr.VCR(
    serializer='yaml',
    cassette_library_dir=str(TESTS_DIR / 'fixtures' / 'adapters' / 'google'),

    # We should NOT record new interactions, as we don't want to make real requests
    # in a CI environment.
    record_mode=settings.CASSETTE_RECORD_MODE,
    # record_mode='new_episodes',

    # Requests using the same URI and method will not be registered.
    match_on=['uri', 'method'],

    # Do not store sensitive information into a JSON file!
    filter_query_parameters=['key']
)
