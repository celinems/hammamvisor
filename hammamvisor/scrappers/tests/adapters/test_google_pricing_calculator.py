import pytest

from scrappers.adapters.google_pricing_calculator import GooglePricingCalculator


class TestGooglePricingCalculator:

    @pytest.fixture(autouse=True, scope='function')
    def set_up(self):
        self.res = GooglePricingCalculator()
        yield
        self.res.clean_class_variables()


    def test_photos_price(self):
        self.res.total_price('photos', 3847)
        assert self.res.SPENT == 23.966810000001534
        assert self.res.REMAINING_BUDGET == self.res.BUDGET - self.res.SPENT
        assert self.res.PHOTOS_DATA == 3847


    def test_place_details_price(self):
        self.res.total_price('place_details', 3847)
        assert self.res.SPENT == 85.59574999999603
        assert self.res.REMAINING_BUDGET == self.res.BUDGET - self.res.SPENT
        assert self.res.PLACE_DETAILS == 3847


    def test_text_search_price(self):
        self.res.total_price('text_search', 3847)
        assert self.res.SPENT == 136.95319999998333
        assert self.res.REMAINING_BUDGET == self.res.BUDGET - self.res.SPENT
        assert self.res.TEXT_SEARCH_DATA == 3847


    def test_find_place_price(self):
        self.res.total_price('find_place', 3847)
        assert self.res.SPENT == 5.820510999999517
        assert self.res.REMAINING_BUDGET == self.res.BUDGET - self.res.SPENT
        assert self.res.FIND_PLACE_DATA == 3847


    def test_budget_in_euros(self):
        assert self.res.budget_euros == self.res.BUDGET * self.res.DOLLAR_RATE

    def test_remaining_budget_euros(self):
        assert self.res.remaining_budget_euros == self.res.BUDGET * self.res.DOLLAR_RATE


    def test_find_place_data_usage(self):
        self.res.data_usage('find_place')
        assert self.res.FIND_PLACE_DATA == 1


    def test_place_details_data_usage(self):
        self.res.data_usage('place_details')
        assert self.res.PLACE_DETAILS == 1


    def test_text_search_data_usage(self):
        self.res.data_usage('text_search')
        assert self.res.TEXT_SEARCH_DATA == 1


    def test_photos_data_usage(self):
        self.res.data_usage('photos')
        assert self.res.PHOTOS_DATA == 1


    def test_allowed_calls(self):
        calls = self.res.allowed_calls('photos', 3000)
        assert calls == 3000

        calls = self.res.allowed_calls('photos', 30000)
        assert calls == 24786
