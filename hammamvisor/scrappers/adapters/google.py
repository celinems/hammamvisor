import os
import requests
import itertools
import copy
import re
import pytz
import datetime as dt
from time import sleep

import configurations

from django.conf import settings


configurations.setup()


####################################################################
################ Global query to Google Places API #################
####################################################################

class PlaceSearchAdapter:
    API_KEY = os.getenv('GOOGLE_API_KEY')

    @staticmethod
    def request_to_dict(spas: list) -> list:
        results = [{
                'provider_id': spa['place_id'],
                'provider': 'google',
                'name': spa['name']}
            for spa in spas]
        return results

    def _paginated_request(self, previous_pages=None, page=0):
        request = requests.get(self.url, params=self.params)
        json_results = request.json()['results']

        self.params.update({
            'pagetoken': request.json().get('next_page_token')
        })

        if page == 0:
            previous_pages = []
            previous_pages.extend(json_results)
            if len(json_results) < 20:
                return previous_pages
            else:
                page += 1
                sleep(2)
                self._paginated_request(previous_pages, page)


        next_page = False
        unique_ids = set(result['place_id'] for result in previous_pages)
        for result in json_results:
            if result['place_id'] not in unique_ids:
                previous_pages.append(result)
                next_page = True

        if next_page:
            page += 1
            self._paginated_request(previous_pages, page)
        return previous_pages


    def __init__(self):
        self.url = f"https://maps.googleapis.com/maps/api/place/textsearch/json"
        self.params = {
            'key': self.API_KEY,
            'type': 'spa',
            'fields': 'place_id',
            'language': 'fr',
            'region': 'fr'
        }

    def get_spas_in_ile_de_france(self):
        cps = settings.ILE_DE_FRANCE_CPS

        results = []
        for cp in cps:
            self.params.update({
                'query': f'spa+{cp}'
            })
            result = self._paginated_request()
            results.extend(result)

        # As we query multiple places, Google results can contain the same bath twice or more.
        # Make sure we return only unique values.
        filtered_results = []
        for key, group in itertools.groupby(results, key=lambda x: x['place_id']):
            filtered_results.append(list(group)[0])
        return self.request_to_dict(filtered_results)


    def get_establishments_by_name(self, name):
        self.params.update({
            'query': f"{name}+Paris"
        })
        results = self._paginated_request()
        return self.request_to_dict(results)



####################################################################
################ Detail query to Google Places API #################
####################################################################
class PlaceDetailsAdapter:
    API_KEY = os.getenv('GOOGLE_API_KEY')

    @staticmethod
    def _translate_opening_hours(opening_hours: list) -> list:
        """
        Opening hours: list.
            Example: {'close': {'day': 0, 'time': '2200'},
                     'open': {'day': 0, 'time': '1000'}},
                    {'close': {'day': 1, 'time': '2300'},
                     'open': {'day': 1, 'time': '1700'}},
                    {'close': {'day': 2, 'time': '2300'},
                     'open': {'day': 2, 'time': '1000'}},
                    {'close': {'day': 3, 'time': '2300'},
                     'open': {'day': 3, 'time': '1000'}},
                    {'close': {'day': 4, 'time': '2300'},
                     'open': {'day': 4, 'time': '1000'}},
                    {'close': {'day': 5, 'time': '2300'},
                     'open': {'day': 5, 'time': '1000'}},
                    {'close': {'day': 6, 'time': '2100'},
                     'open': {'day': 6, 'time': '1000'}}
            Day 0 is Sunday, day 6 is Saturday.

        output: Day 0 is Monday, Day 6 is Sunday
        """
        new = copy.deepcopy(opening_hours)
        for i, item in enumerate(new):
            for key, details in item.items():
                if i == 0:
                    details['day'] = 6
                else:
                    details['day'] = details['day'] - 1

        # Move Sunday to the last position.
        new.append(new[0])
        del new[0]

        return new

    @staticmethod
    def _format_opening_hours(opening_hours: list) -> list:
        """
        Opening hours: list.
            Example: {'close': {'day': 0, 'time': '2200'},
                     'open': {'day': 0, 'time': '1000'}},
                    {'close': {'day': 1, 'time': '2300'},
                     'open': {'day': 1, 'time': '1700'}},
                    {'close': {'day': 2, 'time': '2300'},
                     'open': {'day': 2, 'time': '1000'}},
                    {'close': {'day': 3, 'time': '2300'},
                     'open': {'day': 3, 'time': '1000'}},
                    {'close': {'day': 4, 'time': '2300'},
                     'open': {'day': 4, 'time': '1000'}},
                    {'close': {'day': 5, 'time': '2300'},
                     'open': {'day': 5, 'time': '1000'}},
                    {'close': {'day': 6, 'time': '2100'},
                     'open': {'day': 6, 'time': '1000'}}
            Day 0 is Sunday, day 6 is Saturday.

        output: [{'open': dt.time(10, 00), close: dt.time(22, 00)},
                 {'open': dt.time(17, 00), close: dt.time(23, 00)}
                 ...]
        """
        new = []
        for i, oh in enumerate(opening_hours):
            open_str = oh['open']['time']
            close_str = oh['close']['time']
            opens_at = dt.time(int(open_str[:2]), int(open_str[2:]))
            closes_at = dt.time(int(close_str[:2]), int(close_str[2:]))
            new.append({
                'open': opens_at,
                'close': closes_at
            })
        return new

    @staticmethod
    def _format_reviews(reviews: list) -> list:
        """
        Input:
            {'author_name': 'Damien XXX',
            'author_url': 'agoogleurl',
            'language': 'fr',
            'profile_photo_url': 'agoogleurl/photo.jpg',
            'rating': 4,
            'relative_time_description': 'il y a un mois',
            'text': "Nous avons passé un excellent moment avec ma moitié.
            \nLe lieu est beau, bien pensé. Le personnel est très courtois
            et le gommage était parfait. Tout était vraiment très bien.\n\n",
            'time': 1550219608}
        Output:
            {'author_name': 'Damien XXX',
            'author_picture': 'agoogleurl/photo.jpg',
            'text': 'complete review',
            'rating': 4,
            'date': datetime.datetime(2019, 2, 15, 8, 33, 28)}

        """
        f_reviews = []
        for review in reviews:
            date = dt.datetime.fromtimestamp(review['time'])
            date = pytz.utc.localize(date)
            f_reviews.append(
                {
                    'author_name': review['author_name'],
                    'author_picture': review.get('profile_photo_url'),
                    'text': review['text'],
                    'rating': review['rating'],
                    'date': date
                }
            )
        return f_reviews


    @staticmethod
    def _format_address(address: list, location: dict) -> dict:
        """
        Input: address: [{'long_name': '7',
                                             'short_name': '7',
                                             'types': ['street_number']},
                                            {'long_name': 'Rue de Nemours',
                                             'short_name': 'Rue de Nemours',
                                             'types': ['route']},
                                            {'long_name': 'Paris',
                                             'short_name': 'Paris',
                                             'types': ['locality', 'political']},
                                            {'long_name': 'Paris',
                                            'short_name': 'Paris',
                                            'types': ['administrative_area_level_2',
                                                      'political']},
                                           {'long_name': 'Île-de-France',
                                            'short_name': 'Île-de-France',
                                            'types': ['administrative_area_level_1',
                                                      'political']},
                                           {'long_name': 'France',
                                            'short_name': 'FR',
                                            'types': ['country', 'political']},
                                           {'long_name': '75011',
                                            'short_name': '75011',
                                            'types': ['postal_code']}]
             location: {'lat': 48.8652527, 'lng': 2.3723758}

        Output:
            {'address': '7 Rue de Nemours', 'postal_code': 75011, 'city': 'Paris',
             'latitude': 48.8652527, 'longitude': 2.3723758
            }
        """
        dict_address = {}
        for item in address:
            itype = item['types'][0]
            dict_address[itype] = item['long_name']
        dict_address['address'] = "{nb} {street_name}".format(nb=dict_address['street_number'],
                                                              street_name=dict_address['route'])
        dict_address['city'] = dict_address['locality']
        dict_address['latitude'] = location['lat']
        dict_address['longitude'] = location['lng']
        return dict_address

    @classmethod
    def _format_pictures(cls, pictures: list) -> list:
        """
        Get a list of dictionaries from Google and return a dictionary of attributes
        needed for a Picture object.

        Input:
            [{'height': 533,
            'html_attributions': ['<a href="https://maps.google.com/maps/contrib/100912505352908252306/photos">Hammam '
                                               'Les Cent Ciels Paris</a>'],
            'photo_reference': 'somereference',
            'width': 800}]

        Output:
            [
                {'height': 533,
                'width': 800,
                'provider_id': 'somereference'}
            ]
        """
        if not pictures:
            return []

        return [
            {'height': pict['height'],
             'width': pict['width'],
             'provider_id': pict['photo_reference']}
            for pict in pictures
        ]


    @classmethod
    def _request_to_dict(cls, request: requests.Request) -> dict:
        """
        Name: string
        Opening hours: list.
        Reviews: list
        Address: dict
        """
        result = request.json()['result']

        opening_hours = None
        reviews = None
        address = None
        pictures = None

        if result.get('opening_hours'):
            opening_hours = cls._translate_opening_hours(result['opening_hours']['periods'])
            opening_hours = cls._format_opening_hours(opening_hours)

        if result.get('reviews'):
            reviews = cls._format_reviews(result['reviews'])

        if result.get('address_components'):
            address = cls._format_address(result['address_components'], \
                                        result['geometry']['location'])
        if result.get('photos'):
            pictures = cls._format_pictures(result['photos'])

        return {
            'name': result['name'],
            'opening_hours': opening_hours,
            'reviews': reviews,
            'address': address,
            'pictures': pictures,
            'website': result.get('website', None),
            'phone': result.get('formatted_phone_number', None),
            'rating': result.get('rating', None),
            'total_reviews': result.get('user_ratings_total', None)
        }


    def __init__(self):
        self.url = f"https://maps.googleapis.com/maps/api/place/details/json"
        self.params = {
            'key': self.API_KEY,
            'language': 'fr'
        }

    def get_details(self, id: str) -> dict:
        self.params.update({
            'placeid': id
        })
        request = requests.get(self.url, params=self.params)
        if request.json()['status'] == 'OK':
            spa_details = self._request_to_dict(request)
        else:
            raise Exception

        return spa_details



####################################################################
################ Query to Google Places API Photos #################
####################################################################

class PlacePhotosAdapter:
    API_KEY = os.getenv('GOOGLE_API_KEY')

    def __init__(self):
        self.url = 'https://maps.googleapis.com/maps/api/place/photo'
        self.params = {
            'key': self.API_KEY
        }

    @staticmethod
    def strip_image_type(img: str) -> str:
        """
        Return only the type of an image.

        Input: 'image/png'
        Output: 'png'
        """
        res = re.search(r'^image/(\D+)$', img)
        return res.groups()[0]



    def image_from_photo_reference(self, **picture: dict) -> dict:
        """
        Return the picture binary object from a photo reference.

        Input: {'height': 533,
             'provider_id': 'somereference',
             'width': 800}

        Output {'type': 'jpeg', 'image': b'\x94\x7f\xff\xd9', 'width': 800, 'height': 533}
        """
        self.params.update({
            'maxwidth': picture['width'],
            'photoreference': picture['provider_id']
        })
        request = requests.get(self.url, params=self.params)

        if request.status_code != 200:
            raise Exception

        image_type = self.strip_image_type(request.headers['Content-Type'])
        image = request.content

        return {
            'image': image,
            'type': image_type,
            'width': picture['width'],
            'height': picture['height'],
            'provider_id': picture['provider_id']
        }





#########################################################################
####################### Place Details example result ####################

# {'html_attributions': [],
#  'result': {'address_components': [{'long_name': '7',
#                                     'short_name': '7',
#                                     'types': ['street_number']},
#                                    {'long_name': 'Rue de Nemours',
#                                     'short_name': 'Rue de Nemours',
#                                     'types': ['route']},
#                                    {'long_name': 'Paris',
#                                     'short_name': 'Paris',
#                                     'types': ['locality', 'political']},
#                                    {'long_name': 'Paris',
#                                     'short_name': 'Paris',
#                                     'types': ['administrative_area_level_2',
#                                               'political']},
#                                    {'long_name': 'Île-de-France',
#                                     'short_name': 'Île-de-France',
#                                     'types': ['administrative_area_level_1',
#                                               'political']},
#                                    {'long_name': 'France',
#                                     'short_name': 'FR',
#                                     'types': ['country', 'political']},
#                                    {'long_name': '75011',
#                                     'short_name': '75011',
#                                     'types': ['postal_code']}],
#             'adr_address': '<span class="street-address">7 Rue de '
#                            'Nemours</span>, <span '
#                            'class="postal-code">75011</span> <span '
#                            'class="locality">Paris</span>, <span '
#                            'class="country-name">France</span>',
#             'formatted_address': '7 Rue de Nemours, 75011 Paris, France',
#             'formatted_phone_number': '01 55 28 95 75',
#             'geometry': {'location': {'lat': 48.8652527, 'lng': 2.3723758},
#                          'viewport': {'northeast': {'lat': 48.86663783029149,
#                                                     'lng': 2.373869280291501},
#                                       'southwest': {'lat': 48.8639398697085,
#                                                     'lng': 2.371171319708497}}},
#             'icon': 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png',
#             'id': '30286aba79c11761a2c2d45b4295c2bddf884c0d',
#             'international_phone_number': '+33 1 55 28 95 75',
#             'name': 'Hammam Les Cent Ciels Paris',
#             'opening_hours': {'open_now': True,
#                               'periods': [{'close': {'day': 0, 'time': '2200'},
#                                            'open': {'day': 0, 'time': '1000'}},
#                                           {'close': {'day': 1, 'time': '2300'},
#                                            'open': {'day': 1, 'time': '1700'}},
#                                           {'close': {'day': 2, 'time': '2300'},
#                                            'open': {'day': 2, 'time': '1000'}},
#                                           {'close': {'day': 3, 'time': '2300'},
#                                            'open': {'day': 3, 'time': '1000'}},
#                                           {'close': {'day': 4, 'time': '2300'},
#                                            'open': {'day': 4, 'time': '1000'}},
#                                           {'close': {'day': 5, 'time': '2300'},
#                                            'open': {'day': 5, 'time': '1000'}},
#                                           {'close': {'day': 6, 'time': '2100'},
#                                            'open': {'day': 6, 'time': '1000'}}],
#                               'weekday_text': ['Monday: 5:00 – 11:00 PM',
#                                                'Tuesday: 10:00 AM – 11:00 PM',
#                                                'Wednesday: 10:00 AM – 11:00 PM',
#                                                'Thursday: 10:00 AM – 11:00 PM',
#                                                'Friday: 10:00 AM – 11:00 PM',
#                                                'Saturday: 10:00 AM – 9:00 PM',
#                                                'Sunday: 10:00 AM – 10:00 PM']},
#             'photos': [{'height': 533,
#                         'html_attributions': ['<a '
#                                               'href="https://maps.google.com/maps/contrib/100912505352908252306/photos">Hammam '
#                                               'Les Cent Ciels Paris</a>'],
#                         'photo_reference': 'CmRaAAAA6GvgD-7-tRDI9ChL4vMstpqOgaXHYdc9swor-Z1t8TInPsDAIuW2SZmA30rXBlKcP_DLwkcFmXysceafcac6BUN_NZmQf1h_Rq__QHBi8Z49sWnPZIZmR8eKwoOUXm4iEhA7nxmGQPJkYhgRQnDGpx9xGhQeMd_Puzk_Ubb79De9Xg3wlKHWjQ',
#                         'width': 800},
#                        {'height': 533,
#                         'html_attributions': ['<a '
#                                               'href="https://maps.google.com/maps/contrib/100912505352908252306/photos">Hammam '
#                                               'Les Cent Ciels Paris</a>'],
#                         'photo_reference': 'CmRaAAAA3_-Tsg3XxsPFNeLh4TIm3uB_5oeJLQB4gS2FI1smACMXZ8wr_MkVPSkWSaNJcaonXsDZWmBiebGgfraAtFI62kByc1CWlxgkcs9lfYZdVj-O5x6TCJ6lQ4XJ9zD4fJlaEhDVSQme1hP6MplByu8uX3SJGhSgqFeO4E69FZRo5VSdk97sa_jh8g',
#                         'width': 800},
#                        {'height': 3996,
#                         'html_attributions': ['<a '
#                                               'href="https://maps.google.com/maps/contrib/104422203436248375850/photos">Sonia '
#                                               'Rabhi</a>'],
#                         'photo_reference': 'CmRaAAAA_z-hVgrXokaSrPlK6ncmHnOgZeDdkXCRtuZBRUGdbiHGTc7_EOIBL4Dbo6ZPGplL7E8-MF1IVyL2-oG9fHnAcDMQRdEcM7tJZAhkrB0cgc9v5nEA2CL9gLgvpDUgdnWYEhAto3anZn_vdyom4zysc1ttGhSTWYfHwtcdwfQMStYNeLLxpdyc_A',
#                         'width': 2664},
#                        {'height': 533,
#                         'html_attributions': ['<a '
#                                               'href="https://maps.google.com/maps/contrib/100912505352908252306/photos">Hammam '
#                                               'Les Cent Ciels Paris</a>'],
#                         'photo_reference': 'CmRaAAAA55nYiIsOdMSk2V_3E9lp5Ulzlbfj3aRlvQMEvCjnpKm6ig3dd2WzoBpJPRZhuORAiF-L5FPed0qPX5T3GUf0sJPf_7ZS4DS5GRHCk3j5-2InZK3Rm_9Cl-9Q3gIe8CAOEhDoKqUv0qScfMtScYlpXfsKGhR7Fsi7BO1-EqUKu9Piet_zFPIc4g',
#                         'width': 800},
#                        {'height': 3264,
#                         'html_attributions': ['<a '
#                                               'href="https://maps.google.com/maps/contrib/116546377060200662546/photos">DELPHINE '
#                                               'PLANCON</a>'],
#                         'photo_reference': 'CmRaAAAANCuGFu-v3W7htrohhdIAjbI_x_5QgcXQ9YYuzJUbPc6KdZH9nAhI3KEZwU2-XiZK2tQ68_I_aYpHAhObK30b_djUDvk8oL1HfJj6Z63tkAeG5az4lW5cC3x3lmvCItiFEhDv7R6i_A6Yvd_9rMlCJ1LnGhQ2oVT9fvHSrfmdXJkEiHdvEAE3-w',
#                         'width': 2448},
#                        {'height': 3024,
#                         'html_attributions': ['<a '
#                                               'href="https://maps.google.com/maps/contrib/103113381385392190581/photos">Diana '
#                                               'COTON PELAGE</a>'],
#                         'photo_reference': 'CmRaAAAAuFHUa0uuHbKrukA33AZSUwIxaOna8Mv0AFfzMiqO1KfBgv1xm-7icMoR7Pp0dLRpo6HFbCFDTNP7JYv-D62yTjVw1HqSqMSQWY60jPrWso1Naa9KEUUUlHoE21TXVNGHEhD0xJe8U_kwf0TK2XXaj9bhGhQTje3k9pK84T6tAavdJmB0vQbdZg',
#                         'width': 4032},
#                        {'height': 4032,
#                         'html_attributions': ['<a '
#                                               'href="https://maps.google.com/maps/contrib/104543263351357335097/photos">Valérie '
#                                               'Dubois</a>'],
#                         'photo_reference': 'CmRaAAAA8UD7vaI3OWYFtiW_yoKLzm-TMS6zwipQUk6ncaYfvrruH9KXvnD_PI8lYXhxoiBByKr6WY18jVZnwN2JRAlOEElPx2KOOHHygwdAvTvEpI6AfBMo_Xdcd5Fkp1trBVKUEhDq5LBAeHhM2Rz9LilkGWsQGhSeN8RolNrop2yHU3dSORm6s_3LCw',
#                         'width': 3024},
#                        {'height': 3024,
#                         'html_attributions': ['<a '
#                                               'href="https://maps.google.com/maps/contrib/107476639640162948297/photos">Guylaine '
#                                               'CORDETTE DE BUFFRENIL</a>'],
#                         'photo_reference': 'CmRaAAAAWCiyX6KDEF-QQSoxp-tAW9JFT-viEc7h-GHBEN7_NIgiajOi-S_RvMFrv9QLRqjutT6YKqcXOeGgCBr725U2MpwRyrXdw11tf0Hhaa4w-6wfJeeESP5G21I7BmhE5UgAEhAhIS0RbNHja6ZMCK-pfq_ZGhRy95xx0EMBbWG9O5m5u1LuU3oZoA',
#                         'width': 4032},
#                        {'height': 4032,
#                         'html_attributions': ['<a '
#                                               'href="https://maps.google.com/maps/contrib/103113381385392190581/photos">Diana '
#                                               'COTON PELAGE</a>'],
#                         'photo_reference': 'CmRaAAAAQT6xU7ev41BiJG2TIPj_dRrRJJkHAAube2q3NTnQ8CFwaTGsFwiK1RJUewpIhpVFsR3QL1bUX0vbCGFZwwIJZJbGOMOkJlrtai6TH05zkRO2lbVwaYLNZxoN9FjBmMycEhDUY-WBrVUAtOdVIkeq40qzGhQCsWgfnYjr8tUwcq3WuOsKhlwe_g',
#                         'width': 3024},
#                        {'height': 2031,
#                         'html_attributions': ['<a '
#                                               'href="https://maps.google.com/maps/contrib/109194108758263536152/photos">Tristane '
#                                               'E</a>'],
#                         'photo_reference': 'CmRaAAAABWIo4PrZMF8Id8WAGOU8LQG9k5Dfsx6D08ux2_t3syHFEgECji42Et8vxaqoyIjqG63y1ktTBvQc_LKSZ2RTnLA0yiWvWIRJDflnIfv3b88Ho77VR3aci0I757xEYbY2EhDemgHyIwm20FXAVrdecQA0GhTzk7QhqDF0QAxjHqFZE99ycClCog',
#                         'width': 1080}],
#             'place_id': 'ChIJ_wdOofxt5kcR2Exb7Hf6w3w',
#             'plus_code': {'compound_code': 'V98C+4X Paris, France',
#                           'global_code': '8FW4V98C+4X'},
#             'rating': 4.2,
#             'reference': 'ChIJ_wdOofxt5kcR2Exb7Hf6w3w',
#             'reviews': [{'author_name': 'Maud M',
#                          'author_url': 'https://www.google.com/maps/contrib/100310568185550136667/reviews',
#                          'language': 'en',
#                          'profile_photo_url': 'https://lh3.googleusercontent.com/-D4mI4imqZ9M/AAAAAAAAAAI/AAAAAAAA8uc/YXgTvxBbCUU/s128-c0x00000000-cc-rp-mo-ba4/photo.jpg',
#                          'rating': 5,
#                          'relative_time_description': '5 months ago',
#                          'text': 'As last year, great place to relax :)',
#                          'time': 1539449063},
#                         {'author_name': 'V W',
#                          'author_url': 'https://www.google.com/maps/contrib/101281874200901026961/reviews',
#                          'language': 'en',
#                          'profile_photo_url': 'https://lh6.googleusercontent.com/-iRIB33-mr4o/AAAAAAAAAAI/AAAAAAAAB1M/mV8uZ9dLz78/s128-c0x00000000-cc-rp-mo-ba4/photo.jpg',
#                          'rating': 4,
#                          'relative_time_description': 'a year ago',
#                          'text': 'In the heart of Paris, a place of relax, '
#                                  'similar to some of the best spa I have seen '
#                                  'abroad. There is a small swimming pool, two '
#                                  'hammam, one (very small) sauna and a room to '
#                                  'rest. They are all connected together. The '
#                                  'staff is friendly and very active, you get '
#                                  'towels, tea, shoes (they are very slippery '
#                                  'though, I would advise to bring your own). '
#                                  'The ambience is very relaxing. The '
#                                  '‘initiation’ package allows to stay from '
#                                  'opening to closing times. I find all other '
#                                  'packages way too expensive. \n'
#                                  'The only drawback (noticed more the second '
#                                  'time I went) is that there is no control '
#                                  'when a couple in obvious urgency of having a '
#                                  'public demonstration of their own union '
#                                  'takes control of the swimming pool.',
#                          'time': 1519848571},
#                         {'author_name': 'Rain Lieberman',
#                          'author_url': 'https://www.google.com/maps/contrib/105039516020588026796/reviews',
#                          'language': 'en',
#                          'profile_photo_url': 'https://lh5.googleusercontent.com/-yBkD8o9aqH4/AAAAAAAAAAI/AAAAAAAAAB4/nEo0mrGMrdg/s128-c0x00000000-cc-rp-mo-ba4/photo.jpg',
#                          'rating': 4,
#                          'relative_time_description': 'a year ago',
#                          'text': 'Very relaxing spa. We were there for few '
#                                  'hours, enjoying the different pools and '
#                                  'saunas',
#                          'time': 1516883500},
#                         {'author_name': 'Ambra',
#                          'author_url': 'https://www.google.com/maps/contrib/103408818458131848707/reviews',
#                          'language': 'en',
#                          'profile_photo_url': 'https://lh6.googleusercontent.com/-U_B29RZ-8Zs/AAAAAAAAAAI/AAAAAAAAABM/Ugbb-s85v8s/s128-c0x00000000-cc-rp-mo-ba3/photo.jpg',
#                          'rating': 5,
#                          'relative_time_description': 'a year ago',
#                          'text': 'Very nice and spacious hammam, well '
#                                  'equipped, beautifully decorated.\n'
#                                  '\n'
#                                  'We tried sérénité plus and éclat plus. Both '
#                                  'were very nice, the massage well made as '
#                                  'well as the scrub. The personnel is kind and '
#                                  'professional.\n'
#                                  '\n'
#                                  'Value for money. Recommended.',
#                          'time': 1517066314},
#                         {'author_name': 'Sofia Superlicious',
#                          'author_url': 'https://www.google.com/maps/contrib/110638561425623687126/reviews',
#                          'language': 'en',
#                          'profile_photo_url': 'https://lh4.googleusercontent.com/-5lsBD3MX0fk/AAAAAAAAAAI/AAAAAAAAAPo/6yQ9LHsZCqI/s128-c0x00000000-cc-rp-mo-ba3/photo.jpg',
#                          'rating': 3,
#                          'relative_time_description': 'a year ago',
#                          'text': 'Nice but expensive. Shared female male. Not '
#                                  'very big ',
#                          'time': 1500624426}],
#             'scope': 'GOOGLE',
#             'types': ['spa', 'point_of_interest', 'establishment'],
#             'url': 'https://maps.google.com/?cid=8990304674135100632',
#             'user_ratings_total': 193,
#             'utc_offset': 60,
#             'vicinity': '7 Rue de Nemours, Paris',
#             'website': 'http://paris.hammam-lescentciels.com/'},
#  'status': 'OK'}
