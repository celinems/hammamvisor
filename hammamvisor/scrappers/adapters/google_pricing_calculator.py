import copy

####################################################################
######################### Google API pricing #######################
####################################################################

class GooglePricingCalculator:
    BUDGET = 195
    REMAINING_BUDGET = 195
    SPENT = 0
    ATMOSPHERE_DATA = 0
    CONTACT_DATA = 0
    FIND_PLACE_DATA = 0
    TEXT_SEARCH_DATA = 0
    BASIC_DATA = 0
    PLACE_DETAILS = 0
    PHOTOS_DATA = 0
    DOLLAR_RATE = 0.89


    def __getattr__(self, key):
        """
        Usage: self.budget_euros or self.remaining_budget_euros
        """
        if 'euros' in key:
            keys = key.split('_')
            del keys[-1]
            cls_key = "_".join(keys).upper()
            val = getattr(self, cls_key)
            return val * self.DOLLAR_RATE

        # return self._data[key]


    @classmethod
    def compute_pricing(cls, cost_per_unit: float, counter: int):
        cls.REMAINING_BUDGET -= cost_per_unit
        cls.SPENT += cost_per_unit
        exec(f'cls.{counter} += 1')


    @classmethod
    def convert_to_euros(cls):
        convertion_rate = cls.DOLLAR_RATE
        cls.SPENT = cls.SPENT * convertion_rate
        cls.REMAINING_BUDGET = cls.BUDGET - cls.SPENT


    @classmethod
    def clean_class_variables(cls):
        cls.REMAINING_BUDGET = 195
        cls.SPENT = 0
        cls.ATMOSPHERE_DATA = 0
        cls.CONTACT_DATA = 0
        cls.FIND_PLACE_DATA = 0
        cls.TEXT_SEARCH_DATA = 0
        cls.BASIC_DATA = 0
        cls.PLACE_DETAILS = 0
        cls.PHOTOS_DATA = 0


    def __repr__(self):
        s = """
        remaining_budget: {},
        spent: {}
        """.format(
            self.REMAINING_BUDGET,
            self.SPENT
        )
        return s


    def basic_data_usage(self):
        # address_component, adr_address, alt_id, formatted_address,
        # geometry, icon, id, name, permanently_closed, photo, place_id,
        # plus_code, scope, type, url, utc_offset, vicinity
        # CAN BE USED TO REPLACE GET GOOGLE BATH IDS including name!!!
        self.compute_pricing(0, 'BASIC_DATA')


    def atmosphere_data_usage(self):
        # fields: price_level, rating, review, user_ratings_total
        # used in get_details_of_a_single_bath_from_google
        if self.ATMOSPHERE_DATA < 100000:
            cost_per_unit = 0.005
            cost_per_thousand = 5
        else:
            cost_per_unit = 0.004
            cost_per_thousand = 4

        self.compute_pricing(cost_per_unit, 'ATMOSPHERE_DATA')


    def contact_data_usage(self):
        # fields: formatted_phone_number, international_phone_number, opening_hours, website
        # used in get_details_of_a_single_bath_from_google
        if self.CONTACT_DATA < 100000:
            cost_per_unit = 0.003
            cost_per_thousand = 3
        else:
            cost_per_unit = 0.0024
            cost_per_thousand = 2.40

        self.compute_pricing(cost_per_unit, 'CONTACT_DATA')


    def find_place_data_usage(self):
        # fields: formatted_phone_number, international_phone_number, opening_hours, website
        # used in get_details_of_a_single_bath_from_google
        if self.FIND_PLACE_DATA < 100000:
            cost_per_unit = 0.0017
            cost_per_thousand = 17
        else:
            cost_per_unit = 0.0136
            cost_per_thousand = 13.60

        self.compute_pricing(cost_per_unit, 'FIND_PLACE_DATA')


    def text_search_data_usage(self):
        # any call using Text Search.
        if self.TEXT_SEARCH_DATA < 100000:
            cost_per_unit = 0.032
        else:
            cost_per_unit = 0.0256

        self.compute_pricing(cost_per_unit, 'TEXT_SEARCH_DATA')
        self.basic_data_usage()
        self.contact_data_usage()
        self.atmosphere_data_usage()


    def place_details_data_usage(self):
        # any call to the Places Details API.
        if self.PLACE_DETAILS < 100000:
            cost_per_unit = 0.017
        else:
            cost_per_unit = 0.0136

        self.compute_pricing(cost_per_unit, 'PLACE_DETAILS')
        self.basic_data_usage()
        self.contact_data_usage()
        self.atmosphere_data_usage()

    def photos_data_usage(self):
        # any call to the Places Photos API.
        if self.PHOTOS_DATA < 100000:
            cost_per_unit = 0.007
        else:
            cost_per_unit = 0.0056

        self.compute_pricing(cost_per_unit, 'PHOTOS_DATA')


    def total_price(self, data_usage, nb):
        if data_usage == 'place_details':
            for _ in range(nb):
                self.place_details_data_usage()

        if data_usage == 'text_search':
            for _ in range(nb):
                self.text_search_data_usage()

        if data_usage == 'find_place':
            for _ in range(nb):
                self.find_place_data_usage()

        if data_usage == 'photos':
            for _ in range(nb):
                self.photos_data_usage()

        self.convert_to_euros()

    def data_usage(self, call_type):
        exec(f'self.{call_type}_data_usage()')


    def allowed_calls(self, call_type: str, desired_calls: int):
        calls = 0

        for _ in range(desired_calls):
            if self.REMAINING_BUDGET > 0.5:
                self.data_usage(call_type)
                calls += 1

        return calls


if __name__ == "__main__":
    res = GooglePricingCalculator()
    res.total_price('photos', 3847)
    print(res)
