from celery import Celery
from django.conf import settings
import configurations


configurations.setup()

app = Celery('tasks')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS, related_name='tasks')
app.config_from_object('background_jobs.celeryconfig')
