from celery import Celery
from django.conf import settings
import configurations


configurations.setup()


broker_url = settings.BROKER_URL
result_backend = settings.BROKER_URL

task_serializer = 'json'
result_serializer = 'json'
accept_content = ['json']
timezone = 'Europe/Oslo'
enable_utc = True
worker_concurrency = 12

# echo enables verbose logging from SQLAlchemy.
database_engine_options = {'echo': True}
