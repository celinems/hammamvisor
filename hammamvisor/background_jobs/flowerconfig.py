import os
# from celery import Celery

# Enable debug logging
# logging = 'DEBUG'
# debug = True
persistent = True
port = int(os.getenv('FLOWER_PORT'))
inspect = True
db = 'flowerdb'

auth_provider = 'flower.views.auth.GithubLoginHandler'
auth = os.getenv('FLOWER_OAUTH')
oauth2_key = os.getenv('FLOWER_OAUTH2_KEY')
oauth2_secret = os.getenv('FLOWER_OAUTH2_SECRET')
oauth2_redirect_uri = os.getenv('FLOWER_OAUTH2_REDIRECT_URI')