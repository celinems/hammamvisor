"""
Utility functions or classes to use in models.
"""


from enum import Enum


class EnumForModels(Enum):
    """
    A base class for Enum with useful methods to make them
    easier to handle in our models.
    """

    @classmethod
    def get_value(cls, member: str):
        # usage: OpeningHours.WEEK_DAYS.get_value('monday')
        return cls[member].value[0]

    @classmethod
    def get_object(cls, value, human=None):
        """
        Search for an item into tuple enumerables.
        Input:
            value: string or integer.
            human: optional. If we want to return the translated representation.
        Output: WEEK_DAYS class variable.
        """
        for key, item in cls._value2member_map_.items():
            if value in key:
                i = item
        if human:
            return i.value[1]
        return i
