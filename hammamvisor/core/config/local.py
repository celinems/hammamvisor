import os
import dj_database_url
import subprocess

from .common import Common

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def show_toolbar(request):
        return True

def debug(context):
  return {'debug': True}


class Local(Common):
    DEBUG = True

    # Testing
    INSTALLED_APPS = Common.INSTALLED_APPS
    INSTALLED_APPS += ('debug_toolbar',)

    dbname = os.getenv("POSTGRES_DB")
    dbuser = os.getenv("POSTGRES_USER")
    DATABASES = {
        'default': dj_database_url.config(
            engine='django.contrib.gis.db.backends.postgis',
            default=f'postgres://{dbuser}:@postgres:5432/{dbname}',
            conn_max_age=int(os.getenv('POSTGRES_CONN_MAX_AGE', 600))
        )
    }

    # Mail
    EMAIL_HOST = 'localhost'
    EMAIL_PORT = 1025
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

    ILE_DE_FRANCE_CPS=['75001']

    # Record mode for all tests using VCR.PY.
    CASSETTE_RECORD_MODE = 'once'

    MIDDLEWARE = Common.MIDDLEWARE
    MIDDLEWARE += ('debug_toolbar.middleware.DebugToolbarMiddleware',)

    INTERNAL_IPS = ('127.0.0.1')

    DEBUG_TOOLBAR_CONFIG = {
        'SHOW_TOOLBAR_CALLBACK': show_toolbar,
    }
    SASS_PROCESSOR_ENABLED = True

    TEMPLATES = Common.TEMPLATES

    # TEMPLATE_CONTEXT_PROCESSORS = ('website.context_processors.debug_mode', ) # not working
    TEMPLATES[0]['OPTIONS']['context_processors'] += ['website.context_processors.debug_mode', ]