import os
import dj_database_url
from .common import Common
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class Test(Common):
    INSTALLED_APPS = Common.INSTALLED_APPS
    DEBUG = True
    dbname = os.getenv("POSTGRES_DB")
    dbuser = os.getenv("POSTGRES_USER")

    # postgresql://[user[:password]@][netloc][:port][/dbname][?param1=value1&...]
    DATABASES = {
        'default': dj_database_url.config(
            engine='django.contrib.gis.db.backends.postgis',
            default=f'postgres://{dbuser}:@postgres:5432/{dbname}',
            conn_max_age=int(os.getenv('POSTGRES_CONN_MAX_AGE', 600))
        )
    }

    ILE_DE_FRANCE_CPS = ['75001']