import uuid
import os

from django.db import models
from django.conf import settings
from django.utils.text import slugify
from django.contrib.gis.db import models as gis_models
from django.contrib.gis import geos
# Signals
from django.db.models.signals import post_save
from django.dispatch import receiver

from core.utils.models import EnumForModels


#####################################################################
############################# Models ################################
#####################################################################

class Address(models.Model):
    created_at = models.DateTimeField("créé le", auto_now_add=True)
    updated_at = models.DateTimeField("modifié le", auto_now=True)
    location = gis_models.PointField('localisation')
    address = models.CharField('adresse', max_length=100)
    postal_code = models.IntegerField('code postal')
    city = models.CharField('ville', max_length=50)


    @classmethod
    def easy_point_field(cls, longitude: float, latitude: float) -> geos.Point:
        """
        It's very easy to get confused with PointField as it's not possible
        to pass latitude and longitude with their key.
        This method returns a Point without any possible confusion.
        """
        return geos.Point(longitude, latitude, srid=4326)


    @property
    def latitude(self) -> float:
        return self.location.y


    @property
    def longitude(self) -> float:
        return self.location.x



class Bath(models.Model):

    class PROVIDERS(EnumForModels):
        google = (0, 'Google')


    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField("nom", max_length=100, blank=False, null=False, unique=True)
    description = models.TextField("description", blank=True, null=True)
    total_reviews = models.IntegerField("nombre de commentaires", default=0)
    rating = models.FloatField("note moyenne", default=0)
    created_at = models.DateTimeField("créé le", auto_now_add=True)
    updated_at = models.DateTimeField("modifié le", auto_now=True)
    website = models.URLField("site web", blank=True)
    booking_url = models.URLField("lien pour réserver", blank=True, null=True)
    provider = models.IntegerField(
        'fournisseur',
        choices=[x.value for x in PROVIDERS],
        null=False)
    provider_id = models.CharField('ID fournisseur', max_length=100, unique=True)
    address = models.OneToOneField(Address, on_delete=models.CASCADE, null=True)
    phone = models.CharField("téléphone", max_length=20, blank=True, null=True)

    @property
    def folder_name(self):
        return os.path.join(
            settings.MEDIA_ROOT,
            'baths',
            slugify(self.name)
        )

    def __str__(self):
        return self.name

    @property
    def max_rating(self):
        return 5



class OpeningHour(models.Model):
    class WEEK_DAYS(EnumForModels):
        monday = (0, 'Lundi')
        tuesday = (1, 'Mardi')
        wednesday = (2, 'Mercredi')
        thursday = (3, 'Jeudi')
        friday = (4, 'Vendredi')
        saturday = (5, 'Samedi')
        sunday = (6, 'Dimanche')


    class GENDERS(EnumForModels):
        men = (0, 'Hommes')
        women = (1, 'Femmes')
        mixed = (2, 'Mixte')


    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField("créé le", auto_now_add=True)
    updated_at = models.DateTimeField("modifié le", auto_now=True)
    week_day = models.IntegerField('jour',
        choices=[x.value for x in WEEK_DAYS], null=False)
    opens_at = models.TimeField('ouvre à', null=False)
    closes_at = models.TimeField('ferme à', null=False)
    available_for = models.IntegerField('ouvert pour',
        choices=[x.value for x in GENDERS], null=True)
    bath = models.ForeignKey(Bath, on_delete=models.CASCADE, related_name="opening_hours")



class Review(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField("créé le", auto_now_add=True)
    updated_at = models.DateTimeField("modifié le", auto_now=True)
    bath = models.ForeignKey(Bath, on_delete=models.CASCADE, related_name="reviews")
    author_name = models.CharField('nom', max_length=60, null=False, blank=False)
    author_picture = models.URLField('photo', blank=True, null=True)
    text = models.TextField('commentaire', null=False, blank=False)
    rating = models.FloatField('note', null=False, blank=False)
    date = models.DateTimeField('laissé le', null=False, blank=False)

    def __str__(self):
        return self.author_name


class Picture(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField("créé le", auto_now_add=True)
    updated_at = models.DateTimeField("modifié le", auto_now=True)
    bath = models.ForeignKey(Bath, on_delete=models.CASCADE, related_name="pictures")
    provider_id = models.CharField(max_length=300, null=True, blank=True)
    width = models.IntegerField(null=True, blank=True)
    height = models.IntegerField(null=True, blank=True)
    image = models.ImageField(upload_to='pictures', max_length=500)

    @property
    def bath_folder_path(self):
        return os.path.join(self.bath.folder_name, 'pictures')

    @property
    def url(self):
        return self.image.url.replace(settings.MEDIA_ROOT, '')


##############################################################################
############################ Signals #########################################
##############################################################################

@receiver(post_save, sender=Bath)
def create_folder(sender, instance, created, **kwargs):
    """Create a dedicated folder when a new Bath is created. """
    if created:
        path = instance.folder_name
        if not os.path.exists(path):
            os.mkdir(path)
