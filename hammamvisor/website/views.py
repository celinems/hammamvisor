import operator
import os

from django.shortcuts import get_object_or_404, render
from django.contrib.gis.measure import D

from .models import Bath, Address


def index(request):
    # Find baths per location
    paris = Address.easy_point_field(latitude=48.857061, longitude=2.335938)
    baths_pk = Address.objects.filter(location__distance_lte=(paris, D(km=15))).values('bath')

    baths = Bath.objects.filter(pk__in=baths_pk)\
            .exclude(pictures__image__exact='')\
            .exclude(pictures__isnull=True)

    rated_baths = baths.order_by('-rating').filter(rating__gte=4).order_by('-total_reviews')[:3]

    best_hammams = baths.filter(name__icontains='hammam')\
        .order_by('-rating')\
        .filter(rating__gte=4)\
        .order_by('-total_reviews')[:3]

    best_spas = baths.filter(name__icontains='spa')\
        .order_by('-rating')\
        .filter(rating__gte=4)\
        .order_by('-total_reviews')[:3]

    context = {
        'rated_baths': rated_baths,
        'best_hammams': best_hammams,
        'best_spas': best_spas
    }
    return render(request, 'website/index.html', context)


def detail(request, bath_id):
    bath = get_object_or_404(Bath, pk=bath_id)
    google_api_key = os.getenv('GOOGLE_API_KEY')
    reviews = bath.reviews.all()
    address = bath.address
    longitude = address.longitude
    latitude = address.latitude
    address = '{}, {}, {}'.format(
        address.address, address.postal_code, address.city
    )
    opening_hours = bath.opening_hours.order_by('week_day').all()
    context = {
        'bath': bath,
        'google_api_key': google_api_key,
        'reviews': reviews,
        'address': address,
        'latitude': latitude,
        'longitude': longitude,
        'opening_hours': opening_hours
    }
    return render(request, 'website/detail.html', context)
