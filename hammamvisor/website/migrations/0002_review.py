# Generated by Django 2.1.7 on 2019-03-30 16:16

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='créé le')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='modifié le')),
                ('author_name', models.CharField(max_length=60, verbose_name='nom')),
                ('author_picture', models.URLField(blank=True, verbose_name='photo')),
                ('text', models.TextField(verbose_name='commentaire')),
                ('rating', models.FloatField(verbose_name='note')),
                ('date', models.DateTimeField(verbose_name='laissé le')),
                ('bath', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reviews', to='website.Bath')),
            ],
        ),
    ]
