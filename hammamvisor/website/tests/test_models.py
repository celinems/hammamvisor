import pytz
import datetime as dt

import pytest
from django.db import transaction
from django.db.utils import IntegrityError
from django.utils.text import slugify
from django.contrib.gis.geos import Point
from django.core.exceptions import ValidationError
from ..models import Bath, OpeningHour, Review, Address, Picture



##################################################################################
##################################### Bath #######################################
##################################################################################
@pytest.mark.django_db(transaction=True)
class TestBath:

    @pytest.fixture(autouse=True, scope="function")
    def setUp(self):
        self.attributes = {
            "name": "Les Cent Ciels",
            "description": """
                UN LIEU RAFFINÉ ET SENSUEL
                les Cent Ciels" est un hymne à la beauté des palais d'orient et un hommage à leur mode de vie. Raffinement des matières, élégance des formes, sensualité des parfums, intensité des couleurs, tout ici invite à la relaxation et à la sérénité. Une fois passée la monumentale porte d'entrée, on est immergé sans transition dans la douceur de vivre à l'orientale. Les murs sont revêtus de tadlakt traditionnel, les plafonds sont parés de lustres aux multiples facettes et de lanternes anciennes, les sols rivalisent de beauté entre tapis posés sur des parquets de cèdre et parterres de mosaïques artisanales. Les volutes de parfums enivrants issues d'encensoirs répartis ici et là, escortent un dédale de galeries ornées d'alcôlves. Le clair obscur ambiant, les jeux d'ombres et de lumières fixent une atmosphère enchanteresse teintée de mystère.
                """,
            'total_reviews': 145,
            'rating': 4.5,
            'website': 'https://paris.hammam-lescentciels.com/le-hammam',
            'booking_url': 'https://paris.hammam-lescentciels.com/le-hammam',
            'provider': Bath.PROVIDERS.get_value('google'),
            'provider_id': 'ChIJ_wdOofxt5kcR2Exb7Hf6w3w',
            'phone': '06 11 04 93 01'
        }

        # Run the test
        yield

        # Clean the database between two tests
        Bath.objects.all().delete()


    def test_save_perfect_bath(self):
        bath = Bath.objects.create(**self.attributes)
        for key, value in self.attributes.items():
            assert getattr(bath, key) == value

        assert Bath.objects.count() == 1

    @pytest.mark.skip('validator for empty string needed')
    def test_should_have_a_name(self):
        self.attributes.pop('name')
        bath = Bath.objects.create(**self.attributes)
        assert Bath.objects.count() == 1

        bath.delete()

        self.attributes['name'] = ''
        Bath.objects.create(**self.attributes)
        assert Bath.objects.count() == 1

    def test_booking_url_can_be_empty(self):
        self.attributes.pop('booking_url')
        Bath.objects.create(**self.attributes)
        assert Bath.objects.count() == 1

    def test_default_rating_is_zero(self):
        self.attributes.pop('rating')
        Bath.objects.create(**self.attributes)
        assert Bath.objects.count() == 1
        assert Bath.objects.first().rating == 0

    def test_description_can_be_empty(self):
        self.attributes.pop('description')
        Bath.objects.create(**self.attributes)
        assert Bath.objects.count() == 1


    def test_total_reviews_default_is_zero(self):
        self.attributes.pop('total_reviews')
        Bath.objects.create(**self.attributes)
        assert Bath.objects.count() == 1
        assert Bath.objects.first().total_reviews == 0

    def test_website_can_be_empty(self):
        self.attributes.pop('website')
        Bath.objects.create(**self.attributes)
        assert Bath.objects.count() == 1


    def test_should_have_a_creation_timestamp(self):
        now = dt.datetime.now(tz=pytz.UTC)
        bath = Bath.objects.create(**self.attributes)
        assert (bath.created_at.day, bath.created_at.month, bath.created_at.year) \
                == (now.day, now.month, now.year)

    def test_should_have_an_updated_timestamp(self):
        now = dt.datetime.now(tz=pytz.UTC)
        bath = Bath.objects.create(**self.attributes)
        bath.name = 'Les Cent Ciels Atlantide'
        bath.save()
        assert (bath.updated_at.day, bath.updated_at.month, bath.updated_at.year) \
                == (now.day, now.month, now.year)

    def test_provider_cannot_be_empty(self):
        self.attributes.pop('provider')
        with transaction.atomic():
            with pytest.raises(IntegrityError):
                Bath.objects.create(**self.attributes)
        assert Bath.objects.count() == 0

    @pytest.mark.skip('validator for empty string needed')
    def test_provider_id_cannot_be_empty(self):
        self.attributes.pop('provider_id')
        with pytest.raises(ValidationError):
            Bath.objects.create(**self.attributes)
        assert Bath.objects.count() == 0

    def test_provider_id_should_be_unique(self):
        Bath.objects.create(**self.attributes)
        assert Bath.objects.count() == 1
        with transaction.atomic():
            with pytest.raises(IntegrityError):
                Bath.objects.create(**self.attributes)
        assert Bath.objects.count() == 1

    def test_folder_name(self):
        bath = Bath.objects.create(**self.attributes)
        assert slugify(self.attributes['name']) in bath.folder_name


##################################################################################
############################## OpeningHour #######################################
##################################################################################


@pytest.mark.django_db(transaction=True)
class TestOpeningHour:

    @pytest.fixture(autouse=True, scope="function")
    def setUp(self):
        bath = Bath.objects.create(
            provider= Bath.PROVIDERS.get_value('google'),
            provider_id= 'ChIJ_wdOofxt5kcR2Exb7Hf6w3w'
        )
        self.attributes = {
            "week_day": OpeningHour.WEEK_DAYS.get_value('monday'),
            "opens_at": dt.time(10, 00, 00),
            'closes_at': dt.time(22, 00, 00),
            'available_for': OpeningHour.GENDERS.get_value('women'),
            'bath_id': bath.id

        }

        # Run the test
        yield

        # Clean the database between two tests
        OpeningHour.objects.all().delete()

    def test_perfect_opening_hours(self):
        oh = OpeningHour.objects.create(**self.attributes)
        for key, value in self.attributes.items():
            assert getattr(oh, key) == value

        assert OpeningHour.objects.count() == 1

    def test_should_have_a_creation_timestamp(self):
        now = dt.datetime.now(pytz.UTC)
        oh = OpeningHour.objects.create(**self.attributes)
        assert (oh.created_at.day, oh.created_at.month, oh.created_at.year) \
                == (now.day, now.month, now.year)

    def test_should_have_an_updated_timestamp(self):
        now = dt.datetime.now(pytz.UTC)
        oh = OpeningHour.objects.create(**self.attributes)
        oh.opens_at = dt.time(9, 30)
        oh.save()
        assert (oh.updated_at.day, oh.updated_at.month, oh.updated_at.year) \
                == (now.day, now.month, now.year)


    def test_should_have_a_week_day(self):
        self.attributes.pop('week_day')

        with transaction.atomic():
            with pytest.raises(IntegrityError):
                OpeningHour.objects.create(**self.attributes)
        assert OpeningHour.objects.count() == 0


    def test_should_have_a_closes_at_hour(self):
        self.attributes.pop('closes_at')

        with transaction.atomic():
            with pytest.raises(IntegrityError):
                OpeningHour.objects.create(**self.attributes)
        assert OpeningHour.objects.count() == 0


    def test_should_have_an_opens_at_hour(self):
        self.attributes.pop('opens_at')

        with transaction.atomic():
            with pytest.raises(IntegrityError):
                OpeningHour.objects.create(**self.attributes)
        assert OpeningHour.objects.count() == 0


    def test_available_for_can_be_empty(self):
        self.attributes.pop('available_for')
        OpeningHour.objects.create(**self.attributes)
        assert OpeningHour.objects.count() == 1


    def test_find_week_day_by_number(self):
        week_day_num = OpeningHour.WEEK_DAYS.get_value('monday')
        week_day = OpeningHour.WEEK_DAYS.get_object(week_day_num)
        assert week_day.value[0] == week_day_num
        assert week_day.value[1] == 'Lundi'


    def test_find_week_day_by_day(self):
        week_day = OpeningHour.WEEK_DAYS.get_object('Lundi')
        assert week_day.value[1] == 'Lundi'


    def test_find_week_day_by_number_and_return_human_name(self):
        week_day_num = OpeningHour.WEEK_DAYS.get_value('monday')
        week_day = OpeningHour.WEEK_DAYS.get_object(week_day_num, human=True)
        assert week_day == 'Lundi'


##################################################################################
################################### Review #######################################
##################################################################################


@pytest.mark.django_db(transaction=True)
class TestReview:

    @pytest.fixture(autouse=True, scope="function")
    def setUp(self):
        self.bath = Bath.objects.create(
            provider= Bath.PROVIDERS.get_value('google'),
            provider_id= 'ChIJ_wdOofxt5kcR2Exb7Hf6w3w'
        )
        self.attributes = {
            "author_name": "Gogolito Toutlemonde",
            "author_picture": "https://lh6.googleusercontent.com/-k4URLPUXW2E/AAAAKKKKAAI/\
            AAAAAAAAAAc/6fDB14ZmkbQ/s128-c0x00000000-cc-rp-mo/photo.jpg",
            'text': """
                Nous avons passé un excellent moment avec ma moitié.\nLe lieu est beau, bien pensé. Le personnel est très courtois et le gommage était parfait. Tout était vraiment très bien.\n\nIl manque une étoile pour la taille du sauna qui permet de tenir à 3/4 personnes. Ca n'a pas été gênant lors de notre séjour aussi est-ce peut-être un peu injuste. Et dans le sauna il manque un sablier pour pouvoir suivre le temps (c'est très important pour être toujours entre 5 et 10 minutes), c'est un accessoire qui ne coûte rien à installer et j'ai entendu d'autres personnes en parler. Je recommande son installation comme dans tous les sauna aussi tôt que possible.\nS'il était possible aussi de faire savoir aux gens que ce genre d'établissements appelle au calme, ce serait un grand plus. Partager les lieux avec trois couples qui parlent comme s'ils étaient au bar m'a beaucoup dérangé. Souvent il suffit de glisser un mot lors de la prise des clés pour éviter cette déconvenue.
            """,
            'rating': 4,
            'date': dt.datetime(2019, 3, 8, 22, 00, 00, tzinfo=pytz.UTC),
            'bath_id': self.bath.id

        }

        # Run the test
        yield

        # Clean the database between two tests
        Review.objects.all().delete()


    def test_perfect_review(self):
        review = Review.objects.create(**self.attributes)
        for key, value in self.attributes.items():
            assert getattr(review, key) == value

        assert Review.objects.count() == 1

    def test_should_have_a_creation_timestamp(self):
        now = dt.datetime.now(tz=pytz.UTC)
        review = Review.objects.create(**self.attributes)
        assert (review.created_at.day, review.created_at.month, review.created_at.year) \
                == (now.day, now.month, now.year)

    def test_should_have_an_updated_timestamp(self):
        now = dt.datetime.now(tz=pytz.UTC)
        review = Review.objects.create(**self.attributes)
        review.date = dt.datetime(2019, 5, 3, 19, 9, 30, tzinfo=pytz.UTC)
        review.save()
        assert (review.updated_at.day, review.updated_at.month, review.updated_at.year) \
                == (now.day, now.month, now.year)


    @pytest.mark.skip('validator for empty string needed')
    def test_should_have_an_author_name(self):
        self.attributes.pop('author_name')
        with transaction.atomic():
            with pytest.raises(IntegrityError):
                Review.objects.create(**self.attributes)

        assert Review.objects.count() == 0


    def test_profile_picture_can_be_empty(self):
        self.attributes.pop('author_picture')
        with transaction.atomic():
            Review.objects.create(**self.attributes)

        assert Review.objects.count() == 1


    @pytest.mark.skip('validator for empty string needed')
    def test_should_have_a_comment(self):
        self.attributes.pop('text')
        with transaction.atomic():
            with pytest.raises(IntegrityError):
                Review.objects.create(**self.attributes)

        assert Review.objects.count() == 0


    def test_should_have_a_rating(self):
        self.attributes.pop('rating')
        with transaction.atomic():
            with pytest.raises(IntegrityError):
                Review.objects.create(**self.attributes)

        assert Review.objects.count() == 0


    def test_should_have_a_date(self):
        self.attributes.pop('date')
        with transaction.atomic():
            with pytest.raises(IntegrityError):
                Review.objects.create(**self.attributes)

        assert Review.objects.count() == 0




##################################################################################
################################### Address ######################################
##################################################################################


@pytest.mark.django_db(transaction=True)
class TestAddress:

    @pytest.fixture(autouse=True, scope="function")
    def setUp(self):
        self.bath = Bath.objects.create(
            provider= Bath.PROVIDERS.get_value('google'),
            provider_id= 'ChIJ_wdOofxt5kcR2Exb7Hf6w3w'
        )
        self.location = {
            "longitude": 2.3723758,
            "latitude": 48.8652527
        }
        # Point(longitude, latitude)
        self.attributes = {
            "location": Point(2.3723758, 48.8652527, srid=4326),
            "address": "7 Rue de Nemours",
            "postal_code": 75011,
            "city": "Paris"
        }

        # Run the test
        yield

        # Clean the database between two tests
        Address.objects.all().delete()


    def test_perfect_address(self):
        address = Address.objects.create(**self.attributes)
        for key, value in self.attributes.items():
            assert getattr(address, key) == value

        assert Address.objects.count() == 1


    def test_should_have_a_creation_timestamp(self):
        now = dt.datetime.now(tz=pytz.UTC)
        address = Address.objects.create(**self.attributes)
        assert (address.created_at.day, address.created_at.month, address.created_at.year) \
                == (now.day, now.month, now.year)


    def test_should_have_an_updated_timestamp(self):
        now = dt.datetime.now(tz=pytz.UTC)
        address = Address.objects.create(**self.attributes)
        address.city = 'Montpellier'
        address.save()
        assert (address.updated_at.day, address.updated_at.month, address.updated_at.year) \
                == (now.day, now.month, now.year)


    @pytest.mark.parametrize("attribute", [
        "location", "postal_code"
    ])
    def test_mandatory_attributes(self, attribute):
        self.attributes.pop(attribute)
        with transaction.atomic():
            with pytest.raises(IntegrityError):
                Address.objects.create(**self.attributes)

        assert Address.objects.count() == 0


    @pytest.mark.skip('Validator needed')
    @pytest.mark.parametrize("attribute", [
        "address", "city"
    ])
    def test_mandatory_attributes(self, attribute):
        self.attributes.pop(attribute)
        with transaction.atomic():
            with pytest.raises(IntegrityError):
                Address.objects.create(**self.attributes)

        assert Address.objects.count() == 0


    def test_bath_can_have_an_address(self):
        with transaction.atomic():
            address = Address.objects.create(**self.attributes)
            address.bath = self.bath
            address.save()

        assert Address.objects.count() == 1
        assert address.bath
        assert self.bath.address


    def test_easy_point_field(self):
        point = Address.easy_point_field(latitude=self.location['latitude'],
                                         longitude=self.location['longitude'])
        assert point.x == self.location['longitude']
        assert point.y == self.location['latitude']


    def test_longitude(self):
        address = Address.objects.create(**self.attributes)
        assert address.longitude == address.location.x


    def test_latitude(self):
        address = Address.objects.create(**self.attributes)
        assert address.latitude == address.location.y


##################################################################################
################################## Picture #######################################
##################################################################################
@pytest.mark.django_db(transaction=True)
class TestPicture:

    @pytest.fixture(autouse=True, scope="function")
    def setUp(self):
        self.bath = Bath.objects.create(
            provider= Bath.PROVIDERS.get_value('google'),
            provider_id= 'ChIJ_wdOofxt5kcR2Exb7Hf6w3w',
            name='Les Cent Ciels'
        )
        self.attributes = {
            'width': 400,
            'height': 800,
            'provider_id': 'CmRaAAAA6GvgD-7-tRDI9ChL4vMstpqOgaXHYdc9swor-Z1t8TInPsDAIuW2SZmA30rXBlKcP_DLwkcFmXysceafcac6BUN_NZmQf1h_Rq__QHBi8Z49sWnPZIZmR8eKwoOUXm4iEhA7nxmGQPJkYhgRQnDGpx9xGhQeMd_Puzk_Ubb79De9Xg3wlKHWjQ',
            'bath_id': self.bath.id

        }


        # Run the test
        yield

        # Clean the database between two tests
        self.bath.delete()
        Picture.objects.all().delete()


    def test_perfect_picture(self):
        picture = Picture.objects.create(**self.attributes)
        for key, value in self.attributes.items():
            assert getattr(picture, key) == value

        assert Picture.objects.count() == 1


    def test_picture_belongs_to_a_bath(self):
        Picture.objects.create(**self.attributes)
        assert self.bath.pictures.count() == 1


    @pytest.mark.parametrize('attribute', ['height', 'width', 'provider_id'])
    def test_not_mandatory_attributes(self, attribute):
        self.attributes.pop(attribute)
        Picture.objects.create(**self.attributes)
        assert Picture.objects.count() == 1


    @pytest.mark.parametrize('attribute', ['created_at', 'updated_at', 'id'])
    def test_automatic_attributes(self, attribute):
        picture = Picture.objects.create(**self.attributes)
        assert picture.created_at
        picture.width = 200
        picture.save()
        assert picture.updated_at
        assert picture.id


    def test_folder_path_if_bath(self):
        picture = Picture.objects.create(**self.attributes)
        folder_path = picture.bath_folder_path
        assert picture.bath.folder_name in folder_path