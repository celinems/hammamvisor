from django import template

from ..models import Bath, OpeningHour

register = template.Library()


@register.filter
def remaining_rating(rating):
    """
    Return the remaining rating for this bath.
    Useful when displaying stars to represent a rating.
    """
    rating = float(rating)
    return Bath().max_rating - round(rating)


@register.filter(name='range')
def filter_range(start, end):
    """
    Loop over an integer in templates!
    """
    start = int(start)
    end = int(end)
    return range(start, end)


@register.filter
def week_day_human_name(number: int):
    """
    Show the human, translated name of a week day given its number.
    Input: number
    """
    return OpeningHour.WEEK_DAYS.get_object(number, human=True)
