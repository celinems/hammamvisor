from django.urls import path, re_path

from . import views


urlpatterns = [
    path('<str:bath_id>/', views.detail, name='bath_details'),
]
