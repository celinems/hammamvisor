from django.conf import settings

def ga_code(request):
    # return the value you want as a dictionnary. you may add multiple values in there.
    return {'GA_TRACKING_CODE': settings.GA_TRACKING_CODE}


def debug_mode(request):
    return {'DEBUG_MODE': settings.DEBUG}