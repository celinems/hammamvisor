# Hammamvisor

[![Built with](https://img.shields.io/badge/Built_with-Cookiecutter_Django_Rest-F7B633.svg)](https://github.com/agconti/cookiecutter-django-rest)

Find the spa made for you! Are you going alone or with your friends? Do you want a private hammam or a collective one?
We gathered all spas in Ile-de-france and organized them so you can find the one that fits your needs.


# API

This repository contains the API and back-end related code. The front end is not made yet.
It uses Celery, RabbitMQ, Django Rest Framework, Postgresql and Pytest. No worries, it's all ready out-of-the-box thanks to Docker!


# Front end

To be done! :)


# Prerequisites

- [Docker](https://docs.docker.com/docker-for-mac/install/)  and Docker Compose.


# Local Development

Before you begin, *ask an administrator for the environment variables* or make your own following this example: `docker/global.example.env`.
When you're ready, add a new file in `docker/` called `global.env`:

```
# docker/global.env
DJANGO_SETTINGS_MODULE=example.example
...
```


Start the dev server for local development:
```bash
docker-compose up
```

This will start all services, install dependencies, start a Django server and create a set of data in the database you can play with.

Run a command inside the docker container:

```bash
docker-compose run --rm web [command]
```

You can find useful orther commands in the Makefile.


# Tests

You need to have a `docker/global.env` file before you run the tests. If you don't, contact the repository owner or make your own following `docker/global.example.env`.

This project has a test coverage > 90%. You can see it running `make test-coverage`. *You must have opera installed*.
If you don't have it, the last task will fail but you can still open the report with your favourite brower this way: `mybrowser htmlcov/index.html`.


# To be done

This is a work in progress. Here are the next steps:

- [x] Gather spas from Google (`places-api` branch): details, location (with geocoding) and recommendations.
- [] Gather recommendations from Facebook.
- [] Gather recommendations from TripAdvisor.
- [] Setup a Digital Ocean server to run the API.
- [] Configure a CI.
- [] Configure a CD.
- [] Make the front end.


# Images
Index header: https://www.flickr.com/photos/139553386@N05/30731687484/