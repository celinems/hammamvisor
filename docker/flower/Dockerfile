FROM python:3.6

# Postgis

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
  apt-get dist-upgrade -y && \
  apt-get install -y libgdal-dev && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists*

# Allows docker to cache installed dependencies between builds
COPY ./requirements.txt requirements.txt
RUN pip install -r requirements.txt
RUN pip install tornado==4.5.3

# PYTHONUNBUFFERED: Force stdin, stdout and stderr to be totally unbuffered. (equivalent to `python -u`)
# PYTHONHASHSEED: Enable hash randomization (equivalent to `python -R`)
# PYTHONDONTWRITEBYTECODE: Do not write byte files to disk, since we maintain it as readonly. (equivalent to `python -B`)
ENV PYTHONUNBUFFERED=1 PYTHONHASHSEED=random PYTHONDONTWRITEBYTECODE=1

# Default port
EXPOSE 5555


COPY . code

WORKDIR code


# Run as a non-root user by default, run as user with least privileges.
# USER nobody
