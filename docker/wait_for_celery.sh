#!/bin/sh

until timeout 20 celery -A background_jobs.run inspect ping; do
    >&2 echo "Celery workers not available... Waiting.."
done

echo 'Starting flower'
celery -A background_jobs.run flower --conf=hammamvisor/background_jobs/flowerconfig.py --broker=amqp://docker:password@broker:5672/my_vhost --broker-api=http://docker:password@broker:15672/api/