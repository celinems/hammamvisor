#!/bin/bash
# set -e

echo '######################### Creating databases  #######################################'

for DB in $DB_TO_CREATE
do
    createdb -U $POSTGRES_USER -p $POSTGRES_PORT $DB # -U $POSTGRES_USER
    echo "######################### Creating database ${DB} #######################################"
done
